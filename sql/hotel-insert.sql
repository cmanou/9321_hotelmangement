/* Accounts */
INSERT INTO users (user_name, user_pass) VALUES ('owner', '5f4dcc3b5aa765d61d8327deb882cf99'); /*password*/
INSERT INTO users (user_name, user_pass) VALUES ('manager', '202cb962ac59075b964b07152d234b70');/*123*/
INSERT INTO user_roles (user_name, role_name) VALUES ('owner', 'ownerUser');
INSERT INTO user_roles (user_name, role_name) VALUES ('manager', 'managerUser');

/* Hotels */
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Sydney', 'Sydney');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Wollongong', 'Sydney');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Port Stephens', 'Sydney');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Jervis Bay', 'Sydney');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Byron Bay', 'Sydney');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Liverpool', 'Sydney');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Cronulla', 'Sydney');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Brisbane', 'Brisbane');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Cairns', 'Cairns');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Perth', 'Perth');
INSERT INTO HOTEL(name, location) VALUES ('Bec Hotel Hobart', 'Hobart');

/* Room Types */
INSERT INTO ROOM(type, price, extra_bed_allowed, max_allowed) VALUES ('Single Room (with 1 single bed)', 70, false, 15);
INSERT INTO ROOM(type, price, extra_bed_allowed, max_allowed) VALUES ('Twin Bed (2 single beds)', 120, true,10);
INSERT INTO ROOM(type, price, extra_bed_allowed, max_allowed) VALUES ('Queen (1 double bed) ', 120, true,10);
INSERT INTO ROOM(type, price, extra_bed_allowed, max_allowed) VALUES ('Executive (1 double bed, more facilities than Queen)', 180, true,5);
INSERT INTO ROOM(type, price, extra_bed_allowed, max_allowed) VALUES ('Suite (2 double beds, most luxurious)', 300, true,2);

/* Hotel Sydney */
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1A', 'Available', 1, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1B', 'Available', 1, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1C', 'Available', 1, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1D', 'Under maintenance', 1, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1E', 'Available', 1, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2A', 'Available', 1, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2B', 'Available', 1, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2C', 'Available', 1, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3A', 'Under maintenance', 1, 3);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3B', 'Available', 1, 3);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('4A', 'Available', 1, 4);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('4B', 'Under maintenance', 1, 4);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('5A', 'Available', 1, 5);




/* Hotel Brisbane */
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1A', 'Available', 2, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1B', 'Available', 2, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1C', 'Available', 2, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1D', 'Under maintenance', 2, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1E', 'Available', 2, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2A', 'Available', 2, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2B', 'Available', 2, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2C', 'Available', 2, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3A', 'Under maintenance', 2, 3);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3B', 'Available', 2, 3);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('4A', 'Available', 2, 4);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('4B', 'Under maintenance', 2, 4);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('5A', 'Available', 2, 5);

/* Hotel Cairns */
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1A', 'Available', 3, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1B', 'Available', 3, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2A', 'Available', 3, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2B', 'Available', 3, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3A', 'Under maintenance', 3, 3);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3B', 'Available', 3, 3);

/* Hotel Perth */
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1A', 'Available', 4, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1B', 'Available', 4, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2A', 'Available', 4, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2B', 'Available', 4, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3A', 'Under maintenance', 4, 3);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3B', 'Available', 4, 3);

/* Hotel Hobart */
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1A', 'Available', 5, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('1B', 'Available', 5, 1);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2A', 'Under maintenance', 5, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('2B', 'Available', 5, 2);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3A', 'Under maintenance', 5, 3);
INSERT INTO HOTEL_ROOM(room_number, status, hotel_id, room_id) VALUES ('3B', 'Available', 5, 3);

/* Discounts */
INSERT INTO DISCOUNT (hotel_id,room_id,amount,start_date,end_date) VALUES (1, 1, 30, '01.05.2014', '01.06.2014');
INSERT INTO DISCOUNT (hotel_id,room_id,amount,start_date,end_date) VALUES (3, 3, 20, '01.07.2014', '15.07.2014');

/* Bookings */
/* TODO: Add Bookings








