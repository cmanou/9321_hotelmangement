<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="currentBooking" class="com.groupN.jdbc.BookingDTO" scope="session" />
<jsp:useBean id="resultBean" class="com.groupN.jdbc.ResultBean" scope="session" />

	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Booking Show" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Booking - ${currentBooking.name} </h3>
				
				<br>
				<h3 class="information-head">Current Booked Rooms</h3>
				
				<c:if test="${not empty errors}">
				<br>
				<ul>
				<c:forEach var="error" items="${errors}">
					<li>${error}</li>
				</c:forEach>
				</ul>
				<br>
				</c:if>
				
				<table class="pure-table pure-table-horizontal">
					<thead>
						<tr>
							<th>Hotel</th>
							<th>Room type</th>
							<th>Extra Bed</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="rb" items="${currentBooking.roomBookings}">
						<tr>
							<td>${rb.hotel.name}<br>(${rb.hotel.location})</td>
							<td>${rb.room.type}</td>
							<c:choose>
								<c:when test="${rb.extraBed}">
									<td>Yes</td>
								</c:when>
								<c:otherwise>
									<td>No</td>
								</c:otherwise>
							</c:choose>
						</tr>
						</c:forEach>
					</tbody>
				</table>
				
				<br>Total Price: $ ${currentBooking.price}
				<br><br>
				<form action='/HotelManagement/consumer/searchRoom' method='POST'class="pure-form pure-form-stacked" >
					<input type='submit' value='Add extra room' class="pure-button pure-button-primary">
				</form>
				
            </div>
        </div>
        
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->




<%@ include file="/footer.html"%>


