<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="booking" class="com.groupN.jdbc.BookingDTO" scope="session" />
<jsp:useBean id="resultBean" class="com.groupN.jdbc.ResultBean" scope="session" />
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Confirm Booking" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Hotel Room Booking Cart</h3>
                
<form action='/HotelManagement/consumer/checkout' method='POST' class="pure-form pure-form-stacked">	
<input type="hidden" name="token" value="${token}" > 

<table class="pure-table">
	<thead>
	<tr>
		<th>Hotel</th>
		<th>Room type</th>
		<th>Max Price/Per Night</th>
		<th>TotalPrice</th>
		<th>Exra bed?</th>
	</tr>
	</thead>
	<tbody>
	<c:forEach var="room" items="${resultBean.resultsSelected}" varStatus="status">
		<tr>
			<td>${room.hotel.name}<br>(${room.hotel.location})</td>
			<td>${room.room.type}</td>
			<td><fmt:formatNumber value="${room.maxNightPrice}" type="currency" /></td>
			<td><fmt:formatNumber value="${room.totalPrice}" type="currency" /></td>
			<td></td>
		</tr>
		<c:choose>
			<c:when test="${room.room.extraBedAllowed}">
			<tr>
				<td></td>
				<td>Add extra bed</td>
				<td>$35</td>
				<td></td>
				<td><input name="extraBed" type="checkbox" value="${status.index}"></td>
			</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>Not available</td>
				</tr>
			</c:otherwise>
		</c:choose>			
	</c:forEach>
	</tbody>
</table>
<br>
Total Price: $ ${booking.price}
<br><br>
<input type='submit' value='Proceed to Checkout' class="pure-button pure-button-primary">

</form>

<br>
<form action='/HotelManagement/consumer/cancel' method='POST'>
	<input type='submit' value='Cancel' class="pure-button pure-button-primary">
</form>
</div>
        </div>
  
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->


<%@ include file="/footer.html"%>

