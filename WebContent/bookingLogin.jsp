<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="resultBean" class="com.groupN.jdbc.ResultBean" scope="session" />
 
	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Booking Login" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Booking Login</h3>
				
				<p>${error}</p>
				
				<form action='/HotelManagement/consumer/viewBooking?id=${id}' method='POST' class="pure-form">
					<input type="password" name="password" placeholder="Password">
			
					<input type="submit" value="Login" class="pure-button pure-button-primary">
				 	<input type="reset" class="pure-button">
				</form>
            </div>
        </div>
        
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->




<%@ include file="/footer.html"%>


