<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <jsp:useBean id="currentBooking" class="com.groupN.jdbc.BookingDTO" scope="session" />
<jsp:useBean id="resultBean" class="com.groupN.jdbc.ResultBean" scope="session" />


<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Room search" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Rooms Search Results</h3>
              
<c:choose>
	<c:when test="${empty resultBean.results}">
		<h3>No Rooms Available</h3>
	</c:when>
	
	<c:otherwise>
		<form action='/HotelManagement/consumer/confirmExtraRooms' method='POST'class="pure-form pure-form-stacked" >
	<input type="hidden" name="token" value="${token}" >
		<table class="pure-table">
		<thead>
			<tr>
				<th>Hotel</th>
				<th>Room type</th>
				<th>Max Price/Per Night</th>
				<th>Avg Price/Per Night</th>
				<th>TotalPrice</th>
				<th>Select?</th>
			</tr>
			</thead>
			<tbody>
			<c:forEach var="r" items="${resultBean.results}" varStatus="status">
				<tr>
					<td>${r.hotel.name}<br>(${r.hotel.location})</td>
					<td>${r.room.type}</td>
					<td>${r.maxNightPrice}</td>
					<td>${r.averageNightCost}</td>
					<td>${r.totalPrice}</td>
					<td><input name="roomsSelected" type="checkbox" value="${status.index}"></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<br>
		<input type='submit' value='Add Room' class="pure-button pure-button-primary">
	</form>
	</c:otherwise>
	
</c:choose>              
              
                
			</div>
		</div>
		 <div class="pure-u-1-12"></div>
	</div>
	<br><br><br><br>
</div>	

<%@ include file="/footer.html"%>