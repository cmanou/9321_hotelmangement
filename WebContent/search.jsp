<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="booking" class="com.groupN.jdbc.BookingDTO" scope="session" />
<jsp:useBean id="resultBean" class="com.groupN.jdbc.ResultBean" scope="session" />
	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Search" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Hotel Search</h3>
				
				<c:if test="${not empty errors}">
				<br>
				<ul>
				<c:forEach var="error" items="${errors}">
					<li>${error}</li>
				</c:forEach>
				</ul>
				</c:if>
<br><br>
                
				<form action='/HotelManagement/consumer/searchResults' method='POST'class="pure-form pure-form-stacked" >
				

<label for=city>City</label>
<input type='text' name='city'>
<label for=numRooms>Number of rooms</label>
<input type='text' name='numRooms'>
<label for=maxPrice>Maximum price</label>
$<input type='text' name='maxPrice'>
<br>
<label for=startDate>Starting Date</label>
<input type='text' name='startDate'>
<label for=endDate>Ending Date</label>
<input type='text' name='endDate'>
<br>
<input type='submit' value='search' class="pure-button pure-button-primary"> 

</form>

<br>

    </div>
        </div>
  
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->


<%@ include file="/footer.html"%>

