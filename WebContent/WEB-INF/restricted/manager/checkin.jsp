<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Manager - Hotel Checkin -  ${hotel.name}" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
        <div class="pure-u-1">
            <div class="l-box">
                <h3 class="information-head">Checkin</h3>
				
				
				
Name: ${booking.name }<br>
Email: ${booking.email}<br>
Start Date: ${booking.startDate}<br>
End Date: ${booking.endDate}<br>

<c:forEach var="rb" items="${booking.roomBookings}">
	<ul>
		<li>${rb.room.type} - ${rb.status} 
		
	<c:choose>
	<c:when test="${not empty rb.hotelRoom}">
		 - CheckedIn
	</c:when>
	<c:otherwise>
			<form action='/HotelManagement/manager/checkin?id=${booking.ID }' method='POST'>
			<input type="hidden" name="token" value="${token}" >
			<input type="hidden" value="${rb.ID}" name="booking_room"/>
			<select name="hotel_room">
				<c:forEach var="hr" items="${rb.roomMatches}">		
					<option value="${hr.ID}">${hr.roomNumber}</option>
				</c:forEach>
			</select>	
			<input type='submit' value='Select Room' class="pure-button pure-button-primary">
		</form>
	</c:otherwise>
	</c:choose>
		</li>
		
	</ul>
</c:forEach>
<br>
<a href="/HotelManagement/manager/hotelView?id=${booking.ID}" class="pure-button pure-button-primary">Back to hotel view</a>
            </div>
        </div>
      </div>
     
    <br><br><br><br>
</div> <!-- end l-content -->




<%@ include file="/footer.html"%>    
