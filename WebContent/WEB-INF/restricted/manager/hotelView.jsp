<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <jsp:useBean id="now" class="java.util.Date"/>
 <fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="today" />
    
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Manager - Hotel Checkin -  ${hotel.name}" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
        <div class="pure-u-1">
            <div class="l-box">
                <h3 class="information-head">Current Bookings</h3>
				<c:choose>
					<c:when test="${empty bookings}">
					       <h3>No Current Bookings</h3>
					</c:when>
					<c:otherwise>
				
					<table class="pure-table pure-table-horizontal">
						<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Room</th>
								<th>Tools</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="b" items="${bookings}">
								<tr>
									<td>${b.name}</td>
									<td>${b.email}</td>
									<td>${b.startDate}</td>
									<td>${b.endDate}</td>
									<td>
									<c:forEach var="rb" items="${b.roomBookings}">
										<ul>
											<li>${rb.room.type} - ${rb.status}</li>
										</ul>
									</c:forEach>
									</td>
									<td>
									
									<!--  TODO: Only show one option depending on todays date -->
									<c:if test="${b.startDate eq today}">
										<a href="/HotelManagement/manager/checkin?id=${b.ID}" class="pure-button pure-button-primary">Checkin</a>
									</c:if>
									
									<c:if test="${b.startDate lt today}">
										<c:if test="${b.completelyCheckedin eq false}">
											<a href="/HotelManagement/manager/checkin?id=${b.ID}" class="pure-button pure-button-primary">Checkin</a>
										</c:if>
									</c:if>
									
									<br><bR> 
									<c:if test="${b.endDate ge today}">
										<a href="/HotelManagement/manager/checkout?id=${b.ID}"  class="pure-button">Checkout</a>
									</c:if>
									
									</td>
								</tr>
							</c:forEach>
						</tbody>
						</table>
						
					</c:otherwise>
				</c:choose>
				
            </div>
        </div>
     </div>
     <div class="information pure-g">
        <div class="pure-u-1">
            <div class="l-box">
                <h3 class="information-head">Rooms</h3>
				<c:choose>
					<c:when test="${empty hotel.hotelRooms}">
					<h3>No Rooms</h3>
					</c:when>
					<c:otherwise>
					
					<table class="pure-table pure-table-horizontal">
						<thead>
							<tr>
								<th>Number</th>
								<th>Status</th>
								<th>Type</th>
								<th>Price</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="hr" items="${hotel.hotelRooms}">
							<tr>
								<td>${hr.roomNumber}</td>
								<td>${hr.status}</td>
								<td>${hr.room.type}</td>
								<td><fmt:formatNumber value="${hr.room.price}" type="currency" /></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					
					</c:otherwise>
				</c:choose>
            </div>
        </div>
      </div>
     
    <br><br><br><br>
</div> <!-- end l-content -->




<%@ include file="/footer.html"%>    

