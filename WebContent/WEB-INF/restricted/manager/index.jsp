<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Manager - Select Hotel" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Select a hotel to manage</h3>
                
				<form action='/HotelManagement/manager/hotelView' method='GET' class="pure-form pure-form-stacked" >
				<select name = id>
					<c:forEach var="hotel" items="${hotels}">
					  	<option value="${hotel.ID}">${hotel.name}</option>
					</c:forEach>
				</select>
				
				<br>
				<input type='submit' value='Select' class="pure-button pure-button-primary"> 
				
				</form>
            </div>
        </div>
        
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->


<%@ include file="/footer.html"%>


