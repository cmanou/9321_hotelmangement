<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
  
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Manager - Hotel Checkin -  ${hotel.name}" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
        <div class="pure-u-1">
            <div class="l-box">
                <h3 class="information-head">Checkout</h3>

Name: ${booking.name }<br>
Email: ${booking.email}<br>
Start Date: ${booking.startDate}<br>
End Date: ${booking.endDate}<br>
<c:forEach var="rb" items="${booking.roomBookings}">
	<ul>
		<li>${rb.room.type} - ${rb.status} - ${rb.hotelRoom.status}
		
	<c:choose>
	<c:when test="${rb.hotelRoom.status eq 'Occupied'}">
		 <form action='/HotelManagement/manager/checkout?id=${booking.ID }' method='POST'>
			<input type="hidden" name="token" value="${token}" >
			<input type="hidden" value="${rb.hotelRoom.ID}" name="hotel_room"/>
			<input type='submit' value='Checkout Room' class="pure-button pure-button-primary">
		</form>
	</c:when>
	</c:choose>
		</li>
		
	</ul>
</c:forEach>




            </div>
        </div>
      </div>
     
    <br><br><br><br>
</div> <!-- end l-content -->

<a href="/HotelManagement/manager/hotelView?id=${booking.ID}" class="pure-button pure-button-primary">Back to hotel view</a>


<%@ include file="/footer.html"%> 