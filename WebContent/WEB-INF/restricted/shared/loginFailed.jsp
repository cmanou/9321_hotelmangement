<%-- This is the welcome page and does absolutely nothing other than welcome the user--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Login Failed" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Login Failed</h3>
                
				<p>
				Unknown user name or invalid password or you do not have the correct privileges. Please try
				<a href='/HotelManagement/consumer/logout'>again</a>.
				
				\\TODO: Redirect this properly
				</p>
            </div>
        </div>
        
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->




<%@ include file="/footer.html"%>


