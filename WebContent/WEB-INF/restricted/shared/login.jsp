<%-- This is the welcome page and does absolutely nothing other than welcome the user--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Login" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Login</h3>
                
                <form method="POST" action='<%= response.encodeURL("j_security_check") %>' class="pure-form">
					<input type="text" name="j_username" placeholder="Username">
					<input type="password" name="j_password" placeholder="Password">
			
					<input type="submit" value="Login" class="pure-button pure-button-primary">
				 	<input type="reset" class="pure-button">
				</form>
            </div>
        </div>
        
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->




<%@ include file="/footer.html"%>


