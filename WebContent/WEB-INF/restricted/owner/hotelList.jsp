<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Owner - Hotels" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
    
        <div class="pure-u-1">
            <div class="l-box">
                <h3 class="information-head">Hotel Listings</h3>
                
			<c:choose>
				<c:when test="${empty hotels}">
				       <h3>No Hotels</h3>
				</c:when>
				<c:otherwise>
			
					<table class="pure-table pure-table-horizontal">
					<thead>
						<tr>
							<th>Name</th>
							<th>Location</th>
							<th>Rooms Available</th>
							<th>Rooms Booked</th>
							<th>Occupancy (%)</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="hotel" items="${hotels}">
							<tr>
								<td><a href ="/HotelManagement/owner/showHotel?id=${hotel.ID}">${hotel.name}</a></td>
								<td>${hotel.location}</td>
								<td>${hotel.avaliable}</td>
								<td>${hotel.booked}</td>
								<td>${hotel.occupancy}</td>
							</tr>
						</c:forEach>
					</tbody>
					</table>
					
				</c:otherwise>
			</c:choose>
			<br>
			<a href="/HotelManagement/owner/addHotel" class="pure-button">New Hotel</a>


            </div>
        </div>
        
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->




<%@ include file="/footer.html"%>


