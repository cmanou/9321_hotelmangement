<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    
    
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Owner - Hotel -  ${hotel.name}" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
        <div class="pure-u-1-2">
            <div class="l-box">
                <h3 class="information-head">Room Counts</h3>
				<table class="pure-table pure-table-horizontal">
				<thead>
					<tr>
						<th>Type</th>
						<th>Count</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rc" items="${roomCounts}">
						<tr>
							<td>${rc.key}</td>
							<td>${rc.value}</td>
						</tr>
					</c:forEach>
				</tbody>
				</table>
            </div>
        </div>
        <div class="pure-u-1-2">
            <div class="l-box">
                <h3 class="information-head">Rooms</h3>
				<c:choose>
					<c:when test="${empty hotel.hotelRooms}">
					<h3>No Rooms</h3>
					</c:when>
					<c:otherwise>
					<c:if test="${not empty hotel.notUnderMaintenance}">
						<form action='/HotelManagement/owner/updateStatus?id=${hotel.ID}' method='POST'class="pure-form pure-form-stacked" >
						<table class="pure-table pure-table-horizontal">
							<thead>
								<tr>
									<th>Number</th>
									<th>Status</th>
									<th>Type</th>
									<th>Price</th>
									<th>Maintenance</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="hr" items="${hotel.notUnderMaintenance}">
								<tr>
									<td>${hr.roomNumber}</td>
									<td>${hr.status}</td>
									<td>${hr.room.type}</td>
									<td><fmt:formatNumber value="${hr.room.price}" type="currency" /></td>
									<c:choose>
										<c:when test="${hr.status eq 'Available'}">
											<td><input name="maintenance" type="checkbox" value="${hr.ID}">										
											</td>
										</c:when>
										<c:otherwise>
											<td></td>
										</c:otherwise>
									</c:choose>
								</tr>
								</c:forEach>
							</tbody>
						</table>
	
						<br><br>
						
						<input type='submit' value='Under maintenance' class="pure-button pure-button-primary">
						</form>
					</c:if>
					
					<br><br><br>
					
					<c:if test="${not empty hotel.underMaintenance}">
						<form action='/HotelManagement/owner/updateStatus?id=${hotel.ID}' method='POST'class="pure-form pure-form-stacked" >
						<table class="pure-table pure-table-horizontal">
							<thead>
								<tr>
									<th>Number</th>
									<th>Status</th>
									<th>Type</th>
									<th>Price</th>
									<th>Available</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="hr" items="${hotel.underMaintenance}">
								<tr>
									<td>${hr.roomNumber}</td>
									<td>${hr.status}</td>
									<td>${hr.room.type}</td>
									<td><fmt:formatNumber value="${hr.room.price}" type="currency" /></td>
									<td><input name="maintenance" type="checkbox" value="${hr.ID}">
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
					
						<br><br>
						<input type='submit' value='Make available' class="pure-button pure-button-primary">
						</form>
					</c:if>
					
					</c:otherwise>
				</c:choose>
            </div>
        </div>
      </div>
        <div class="information pure-g">
        <div class="pure-u-1-2">
            <div class="l-box">
                <h3 class="information-head">Current Discounts</h3>
<c:choose>
	<c:when test="${empty hotel.currentDiscounts}">
	       <h3>No Discounts</h3>
	</c:when>
	<c:otherwise>

				<table class="pure-table pure-table-horizontal">
		<thead>
			<tr>
				<th>Room</th>
				<th>Amount</th>
				<th>Start Date</th>
				<th>End Date</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="hd" items="${hotel.currentDiscounts}">
				<tr>
					<td>${hd.room.type}</td>
					<td>${hd.amount}%</td>
					<td>${hd.startDate}</td>
					<td>${hd.endDate}</td>
				</tr>
			</c:forEach>
		</tbody>
		</table>
		
	</c:otherwise>
</c:choose>

            </div>
        </div>
        <div class="pure-u-1-2">
                    <div class="l-box">
                        <h3 class="information-head">Tools</h3>
        
        	<a href="/HotelManagement/owner/newDiscount" class="pure-button">New Discount</a>
		<a href="/HotelManagement/owner/addRoom" class="pure-button">Add a Room</a>
        </div>
        </div>

    </div> <!-- end information -->        
    <br><br><br><br>
</div> <!-- end l-content -->




<%@ include file="/footer.html"%>    
