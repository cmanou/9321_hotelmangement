<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Owner - New Discount" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Add Room</h3>
                
				<form action='/HotelManagement/owner/newDiscount' method='POST'class="pure-form pure-form-stacked" >
				
				<input type="hidden" name="token" value="${token}" > 
				
				<label for=amount>Amount in %</label>
				<input type='text' name='amount'>
				<label for=startDate>Starting Date</label>
				<input type='text' name='startDate'>
				<label for=endDate>Ending Date</label>
				<input type='text' name='endDate'>
				
				<label for=room_type>RoomType</label>
				<select name = room_type>
					<c:forEach var="room" items="${rooms}">
					  	<option value="${room.ID}">${room.type}</option>
					</c:forEach>
				</select>
				<label for=hotel>Hotel</label>
				<select name = hotel>
					<c:forEach var="hotel" items="${hotels}">
					  	<option value="${hotel.ID}">${hotel.name}</option>
					</c:forEach>
				</select>
				
				<br>
				<input type='submit' value='Save' class="pure-button pure-button-primary"> 
				
				</form>
            </div>
        </div>
        
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->


<%@ include file="/footer.html"%>


