<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Owner - Add Hotel" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">New Hotel</h3>
                
                <ul>
				<c:forEach var="error" items="${errors}">
					<li>${error}</li>
				</c:forEach>
				</ul>
                
				<form action='/HotelManagement/owner/addHotel' method='POST'class="pure-form pure-form-stacked" >
				
				<input type="hidden" name="token" value="${token}" > 
				
				<label for=name>Name</label>
				<input type='text' name='name'>
				<label for=location>Location</label>
				<input type='text' name='location'>
				
				<br>
				<input type='submit' value='Save' class="pure-button pure-button-primary"> 
				
				</form>
            </div>
        </div>
        
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->




<%@ include file="/footer.html"%>


