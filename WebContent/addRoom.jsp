<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <jsp:useBean id="currentBooking" class="com.groupN.jdbc.BookingDTO" scope="session" />
<jsp:useBean id="resultBean" class="com.groupN.jdbc.ResultBean" scope="session" />
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Confirm Booking" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Hotel Booking Confirm</h3>
                
    <table class="pure-table">
	<thead>
	<tr>
		<th>Hotel</th>
		<th>Room type</th>
		<th>Total Price</th>
		<th>Extra Beds</th>
	</tr>
	</thead>
	<tbody>
	<c:forEach var="room" items="${resultBean.resultsSelected}">
		<tr>
			<td>${rb.hotel.name}<br>(${room.hotel.location})</td>
			<td>${room.room.type}</td>
			<td><fmt:formatNumber value="${room.totalPrice}" type="currency" /></td>
			<c:choose>
				<c:when test="${room.extraBed}">
					<td>Yes</td>
				</c:when>
				<c:otherwise>
					<td>No</td>
				</c:otherwise>
			</c:choose>
		</tr>
	</c:forEach>
	</tbody>
</table>
<br><br>
Total price of booking + new rooms: Total Price: $ ${booking.price} 

  <br><br> 
 <form action='/HotelManagement/consumer/bookRooms?id=${currentBooking.ID}' method='POST' class="pure-form pure-form-stacked">	
 <input type="hidden" name="token" value="${token}" > 
  <input type='submit' value='Book extra rooms' class="pure-button pure-button-primary">
 </form> 
 
 <br>
 <form action='/HotelManagement/consumer/cancelRoom?id=${currentBooking.ID}' method='POST' class="pure-form pure-form-stacked">	
 <input type="hidden" name="token" value="${token}" > 
 <input type='submit' value='Cancel' class="pure-button pure-button-primary">
 </form>
                              
			</div>
        </div>
        <div class="pure-u-1-12"></div>
    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->


<%@ include file="/footer.html"%>

	