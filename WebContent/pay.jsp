<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="booking" class="com.groupN.jdbc.BookingDTO" scope="session" />
	<jsp:useBean id="resultBean" class="com.groupN.jdbc.ResultBean" scope="session" />
	
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Booking Show" />
</jsp:include>

<div class="l-content">
    <div class="information pure-g">
         <div class="pure-u-1-12"></div>
    
        <div class="pure-u-5-6">
            <div class="l-box">
                <h3 class="information-head">Enter payment details</h3>
			
			<c:if test="${not empty errors}">
				<br>
				<ul>
				<c:forEach var="error" items="${errors}">
					<li>${error}</li>
				</c:forEach>
				</ul>
				</c:if>
			
			<form action='/HotelManagement/consumer/paymentMade' method='POST'class="pure-form pure-form-stacked" >
			<input type="hidden" name="token" value="${token}" > 
			
			Name: <input type="text" name="name"><br>
  			<br>
  			Email: <input type="text" name="email"><br>	
				
			<br><br>
				<input type='submit' value='Submit payment' class="pure-button pure-button-primary">
			</form>
			
			<br>
		<form action='/HotelManagement/consumer/cancel' method='POST'>
					<input type="hidden" name="token" value="${token}" > 
		
			<input type='submit' value='Cancel' class="pure-button pure-button-primary">
		</form>
		
		
				
			</div>
        </div>
        
        <div class="pure-u-1-12"></div>
        

    </div> <!-- end information -->
    <br><br><br><br>
</div> <!-- end l-content -->


<%@ include file="/footer.html"%>