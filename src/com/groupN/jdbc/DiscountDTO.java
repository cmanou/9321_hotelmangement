package com.groupN.jdbc;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class DiscountDTO implements Serializable {
	private static final long serialVersionUID = 463051340648485037L;
	
	private int id;
	private HotelDTO hotel;
	private RoomDTO room;
	private float amount;
	private Date startDate;
	private Date endDate;

	
	public DiscountDTO(int id, float discount,
			Date startDate, Date endDate) {

		this.setId(id);
		this.setAmount(discount);
		this.setStartDate(startDate);
		this.setEndDate(endDate);
	}
	
	public DiscountDTO(float discount,
			Date startDate, Date endDate) {

		this.setAmount(discount);
		this.setStartDate(startDate);
		this.setEndDate(endDate);
	}
	
	public DiscountDTO() {
		
	}

	public HotelDTO getHotel() {
		return hotel;
	}

	public void setHotel(HotelDTO hotel) {
		this.hotel = hotel;
	}

	public RoomDTO getRoom() {
		return room;
	}

	public void setRoom(RoomDTO room) {
		this.room = room;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float discount) {
		this.amount = discount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
