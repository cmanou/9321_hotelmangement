package com.groupN.jdbc;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.sun.tools.javac.util.Pair;

public class RoomDTO {

	private String type; 
	private float price;
	private int ID;
	private boolean extraBedAllowed;
	private List<Pair<Date, Date>> peak;
	private DateFormat df;
	
	public RoomDTO() {
		df = new SimpleDateFormat("dd-MM");
		try {
			peak.add(new Pair(df.parse("15-12"), df.parse("15-02")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	
	public RoomDTO(int id, String type, float price, boolean extraBedAllowed) {
		this.type = type;
		this.ID = id;
		this.price = price;
		this.extraBedAllowed = extraBedAllowed;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public float getPrice() {
		//TODO if off peak
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}

	private float totalPrice(Date startDate, Date endDate) {
		for (Pair p : peak) {
			
		}
		
		return 0;
	}


	public boolean getExtraBedAllowed() {
		return extraBedAllowed;
	}


	public void setExtraBedAllowed(boolean extraBedAllowed) {
		this.extraBedAllowed = extraBedAllowed;
	}
}
