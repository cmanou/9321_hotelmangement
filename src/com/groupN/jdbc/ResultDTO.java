package com.groupN.jdbc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ResultDTO {
	
	private int hotelID;
	private int roomID;
	private HotelDTO hotel;
	private RoomDTO room;
	
	private Date startDate;
	private Date endDate;
	
	private double totalPrice;
	private double maxNightPrice;
		
	private boolean extraBed;
	
	public ResultDTO(int hotelID, int roomID) {
		this.hotelID = hotelID;
		this.roomID = roomID;
	}
	
	public int getHotelID() {
		return hotelID;
	}
	public void setHotelID(int hotelID) {
		this.hotelID = hotelID;
	}
	public int getRoomID() {
		return roomID;
	}
	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}
	public HotelDTO getHotel() {
		return hotel;
	}
	public void setHotel(HotelDTO hotel) {
		this.hotel = hotel;
	}
	public RoomDTO getRoom() {
		return room;
	}
	public void setRoom(RoomDTO room) {
		this.room = room;
	}
	public double getTotalPrice() {
		double bed = 0;
		if(extraBed) {
			bed = 35 * this.getNumberNights();
		}
		return totalPrice + bed;
	}
	public void setTotalPrice(double totalPrice) {

		this.totalPrice = totalPrice;
	}
	public boolean isExtraBed() {
		return extraBed;
	}
	public void setExtraBed(boolean extraBed) {
		this.extraBed = extraBed;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public int getNumberNights() {
		return (int)( (endDate.getTime() - startDate.getTime())  / (1000 * 60 * 60 * 24) );
	}
	
	public double getAverageNightCost() {
		return totalPrice/getNumberNights();
	}
	
	

	public static List<ResultDTO> subtractResultLists(
			List<ResultDTO> possibleRooms,
			List<ResultDTO> conflictingBookingRooms) {
		
		//System.out.println("Count PR" + possibleRooms.size());
		//System.out.println("Count CBR" + conflictingBookingRooms.size());

		
		List<ResultDTO> result = new ArrayList<ResultDTO>(possibleRooms);
		
        for (ResultDTO pr : possibleRooms) {
        	
        	for(ResultDTO cb : conflictingBookingRooms) {
        		if(pr.hotelID == cb.hotelID && pr.roomID == cb.roomID) {
        			result.remove(pr);
                	conflictingBookingRooms.remove(cb);
                	break;
        		}
        	}

        }
		
		//System.out.println("Count FR" + result.size());

		return result;
	}

	public static List<ResultDTO> filterPrice(List<ResultDTO> possibleRooms,
			float maxPrice) {
		
		List<ResultDTO> results = new ArrayList<ResultDTO>();
		
		for(ResultDTO r : possibleRooms) {
			if(r.maxNightPrice <= maxPrice) {
				results.add(r);
			}
		}
		
		return results;
	}

	public static List<ResultDTO> filterCountForHotel(
			List<ResultDTO> possibleRooms, int numRooms) {
		
		Map<Integer,List<ResultDTO>> hotelLists = new HashMap<Integer,List<ResultDTO>>();
		
		for(ResultDTO r : possibleRooms) {
		    if(hotelLists.containsKey(r.getHotelID())) {
		        //Add to existing list
		    	hotelLists.get(r.getHotelID()).add(r);

		    } else {
		        //Create new list
		        List<ResultDTO> results = new ArrayList<ResultDTO>();
		        results.add(r);
		        hotelLists.put(r.getHotelID(), results);
		    }
		}
		
		List<ResultDTO> results = new ArrayList<ResultDTO>();
		

		for(Entry<Integer, List<ResultDTO>> entry : hotelLists.entrySet()) {
			System.out.println("HotelID: " + entry.getKey() + " - " + entry.getValue().size());
			if(entry.getValue().size() >= numRooms) {
				results.addAll(entry.getValue());
			}
		}
		
		
		
		return results;
	}

	public double getMaxNightPrice() {
		return maxNightPrice;
	}

	public void setMaxNightPrice(double maxNightPrice) {
		this.maxNightPrice = maxNightPrice;
	}
	
}
