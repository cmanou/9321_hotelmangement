package com.groupN.jdbc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookingDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private String email;
	private int ID;
	private String pin;
	private Date startDate;
	private Date endDate;
	private int price;
	
	private List<BookingRoomDTO> roomBookings;
	
	public BookingDTO(int id, String name, String email, String pin,
			Date startDate, Date endDate, int price) {

		this.ID = id;
		this.name = name;
		this.email=email;
		this.startDate = startDate;
		this.pin = pin;
		this.endDate = endDate;
		this.price = price;
	}
	
	public BookingDTO() {
		roomBookings = new ArrayList<BookingRoomDTO>();
		price = 0;

	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public List<BookingRoomDTO> getRoomBookings() {
		return roomBookings;
	}
	public void setRoomBookings(List<BookingRoomDTO> roomBookings) {
		this.roomBookings = roomBookings;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public int getPrice() {
		return price;
	}
	
	public boolean getCompletelyCheckedin(){
		for (BookingRoomDTO br : roomBookings) {
			if (br.getStatus().equals("Not Checkedin")) {
				return false;
			}
		}
		
		return true;
	}

	public void add(BookingRoomDTO br) {
		roomBookings.add(br);
		
	}

}
