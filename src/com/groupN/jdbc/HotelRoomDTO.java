package com.groupN.jdbc;

import java.sql.SQLException;
import java.util.Date;
import com.groupN.exceptions.ServiceLocatorException;

public class HotelRoomDTO {
	private HotelDTO hotel;
	private RoomDTO room;
	private String roomNumber;
	private String status;
	private int ID;
	
	public HotelRoomDTO(int id, String roomNumber, String status) {
			this.ID = id;
			this.roomNumber = roomNumber;
			this.status = status;
	}
	
	public HotelRoomDTO(String roomNumber, String status) {
		this.roomNumber = roomNumber;
		this.status = status;
	}

	public HotelDTO getHotel() {
		return hotel;
	}
	public void setHotel(HotelDTO hotel) {
		this.hotel = hotel;
	}
	public RoomDTO getRoom() {
		return room;
	}
	public void setRoom(RoomDTO room) {
		this.room = room;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
	

	public boolean bookedToday() {
		return status.equals("Occupied");
	}
	
	

}
