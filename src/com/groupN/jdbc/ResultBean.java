package com.groupN.jdbc;

import java.util.ArrayList;
import java.util.List;

public class ResultBean {

	private List<ResultDTO> results;
	private List<Integer> selected;
	
	public ResultBean() {
		results = new ArrayList<ResultDTO>();
		selected = new ArrayList<Integer>();
	}
	
	public List<ResultDTO> getResults() {
		return results;
	}
	
	public List<ResultDTO> getResultsSelected() {
		List<ResultDTO>resultsSelected = new ArrayList<ResultDTO>();
		
		for(Integer i: selected) {
			resultsSelected.add(results.get(i));
		}
		
		return resultsSelected;
	}

	public void setResults(List<ResultDTO> results) {
		this.results = results;
	}
	
	public ResultDTO getResult(int index) {
		return results.get(index);
	}
	
	public void setResult(int index, ResultDTO result) {
		results.set(index, result);
	}

	public List<Integer> getSelected() {
		return selected;
	}

	public void setSelected(List<Integer> selected) {
		this.selected = selected;
	}

}
