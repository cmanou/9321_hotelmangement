package com.groupN.jdbc;

import java.util.List;

public class BookingRoomDTO {
	private int ID;
	private HotelDTO hotel;
	private RoomDTO room;
	private HotelRoomDTO hotelRoom;
	private BookingDTO booking;
	private boolean extraBed; //not on single room
	
	public BookingRoomDTO(int id, boolean extraBed) {
		this.ID = id;
		this.extraBed = extraBed;
	}
	
	public BookingRoomDTO() {
		
	}
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public HotelDTO getHotel() {
		return hotel;
	}
	public void setHotel(HotelDTO hotel) {
		this.hotel = hotel;
	}
	public RoomDTO getRoom() {
		return room;
	}
	public void setRoom(RoomDTO room) {
		this.room = room;
	}
	public HotelRoomDTO getHotelRoom() {
		return hotelRoom;
	}
	public void setHotelRoom(HotelRoomDTO hotelRoom) {
		this.hotelRoom = hotelRoom;
	}
	public BookingDTO getBooking() {
		return booking;
	}
	public void setBooking(BookingDTO booking) {
		this.booking = booking;
	}
	public boolean isExtraBed() {
		return extraBed;
	}
	public void setExtraBed(boolean extraBed) {
		this.extraBed = extraBed;
	}
	
	public String getStatus() {
		if(hotelRoom!=null) {
			return "Allocated Room (" + hotelRoom.getRoomNumber() +")"; 
		} else {
			return "Not Checkedin";
		}
	}
	
	public List<HotelRoomDTO> getRoomMatches() {
		System.out.println("ROOM TYPE: " + room.getType());
		return hotel.avaliableRooms(room.getType());
	}


}
