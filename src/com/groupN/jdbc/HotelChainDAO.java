package com.groupN.jdbc;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.groupN.exceptions.EmptyResultException;

public interface HotelChainDAO {
	
	public List<HotelDTO> findAll();
	
	public List<RoomDTO> getRoomTypes();

	public void newHotel(HotelDTO hotel);
	
	public HotelDTO findHotel(int hotelID) throws EmptyResultException;
	
	public List<ResultDTO> findAvaliablitly(String city, Date startDate , Date endDate, float maxPrice, int numRooms );
	
	public BookingDTO getBooking(int ID) throws EmptyResultException;
	
	public List<BookingDTO> getBookingsFromHotel(int HotelID);
	
	public int newBooking(BookingDTO booking);
	
	public void checkInRoom(int roomBookingID, int hotelRoomID);
	
	public void updateBooking(List<ResultDTO> booking, int id, int price);
	
	public void addRoomToHotel(HotelRoomDTO HotelRoom);
	
	public List<HotelRoomDTO> getRoomsFromHotel(int HotelID);
	
	public RoomDTO findRoom(int roomID) throws EmptyResultException;
	
	public DiscountDTO getDiscount(int id) throws EmptyResultException;

	public HotelRoomDTO findHotelRoom(int id) throws EmptyResultException;

	public boolean hotelRoomBooked(int hotelRoomID, Date date);

	public Map<String, Integer> roomTypeCountHotel(int ID);

	public void checkOutRoom(int hotelRoomID);

	void newDiscount(DiscountDTO discount);

	public List<BookingDTO> getCurrentBookingsForHotel(int hotelID);

	public boolean canAddRoomToHotel(int roomID, int hotelID);

	public boolean canEditBooking(int hotelID);
	
	public void updateRoomStatus(int hotelRoomID, String status);

	public boolean goodToken(String token);

	public String newFormToken(String string);
	
	
}
