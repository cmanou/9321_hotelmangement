package com.groupN.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;

import com.groupN.common.Magic;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;

public class DerbyDAOImpl implements HotelChainDAO {

	static Logger logger = Logger.getLogger(DerbyDAOImpl.class.getName());
	private Connection connection;
	DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
	
	public DerbyDAOImpl() throws ServiceLocatorException, SQLException{
		connection = DBConnectionFactory.getConnection();
		//logger.info("Got connection");
	}
	

	@Override
	public void newHotel(HotelDTO hotel) {
		PreparedStatement stmnt = null; 
		
		try{
			String sqlStr = 
				"INSERT INTO HOTEL (NAME, LOCATION) "+
				"VALUES (?,?)";
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setString(1, hotel.getName());
			stmnt.setString(2,  hotel.getLocation());

			//logger.info("sql string is "+stmnt.toString());
			int result = stmnt.executeUpdate();
			//logger.info("Statement successfully executed "+result);
			stmnt.close();
		}catch(Exception e){
			//logger.severe("Unable to store hotel! ");
			e.printStackTrace();
		}
		
	}

	@Override
	public HotelDTO findHotel(int hotelID) throws EmptyResultException {
		HotelDTO hotel = null;
		try{
			//logger.info("The value passed is: "+hotelID);
			
			
			String query_cast = "SELECT * FROM HOTEL WHERE ID = ?";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setInt(1, hotelID);
			ResultSet res = stmnt.executeQuery();
			res.next();
			int id = res.getInt("id");
			String name = res.getString("name");
			String location = res.getString("location");
			System.out.println("Execute Query fh");

			//logger.info(id+" "+name+" "+location);

			hotel = new HotelDTO(id, name, location);
			
			//GET ROOMS FOR HOTEL
			List<HotelRoomDTO> rooms = new ArrayList<HotelRoomDTO>();
			HashMap<Integer, RoomDTO> roomscache = new HashMap<Integer, RoomDTO>();

			try {
				String query = "SELECT * FROM HOTEL_ROOM WHERE HOTEL_ID = ?";
				PreparedStatement stmnt1 = connection.prepareStatement(query);
				stmnt1.setLong(1, hotelID);
				ResultSet res1 = stmnt1.executeQuery();
				System.out.println("Execute Query fh hr");

				//logger.info("The result set size is "+res1.getFetchSize());
				while(res1.next()){
					int id1 = res1.getInt("ID");
					String roomNumber = res1.getString("ROOM_NUMBER");
					String status = res1.getString("STATUS");
					int rooomID = res1.getInt("ROOM_ID");

					//logger.info(id1+" "+roomNumber+" "+status);
					HotelRoomDTO hr = new HotelRoomDTO(id1, roomNumber, status);
					
					hr.setHotel(hotel);
					if(!roomscache.containsKey(rooomID)) {
						roomscache.put(rooomID, findRoom(rooomID));
					}
					hr.setRoom(roomscache.get(rooomID));
					
					
					rooms.add(hr);				
				}
				res1.getStatement().close();
				res1.close();
			}catch(Exception e){
				//System.out.println("Caught Exception");
				e.printStackTrace();
			}
			
			
			hotel.setHotelRooms(rooms);
			
			//GET DISCOUNTS FOR HOTELS
			List<DiscountDTO> discounts = new ArrayList<DiscountDTO>();

			try {
				String query = "SELECT * FROM DISCOUNT WHERE HOTEL_ID = ?";
				PreparedStatement stmnt1 = connection.prepareStatement(query);
				stmnt1.setLong(1, hotelID);
				System.out.println("Execute Query fh d");
				ResultSet res1 = stmnt1.executeQuery();
				//logger.info("The result set size is "+res1.getFetchSize());
				while(res1.next()){
					int id1 = res1.getInt("ID");
					float amount = res1.getFloat("AMOUNT");
					Date startDate = res1.getDate("START_DATE");
					Date endDate = res1.getDate("END_DATE");
					int rooomID = res1.getInt("ROOM_ID");

					DiscountDTO d = new DiscountDTO(id1, amount, startDate, endDate);
					
					d.setHotel(hotel);
					
					if(!roomscache.containsKey(rooomID)) {
						roomscache.put(rooomID, findRoom(rooomID));
					}
					d.setRoom(roomscache.get(rooomID));
					
					
					
					discounts.add(d);				
				}
			}catch(Exception e){
				//System.out.println("Caught Exception");
				e.printStackTrace();
			}
			
			
			hotel.setDiscounts(discounts);;
			res.getStatement().close();
			res.close();
			
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
			throw new EmptyResultException();
		}
		
		return hotel;
	}

	@Override
	public List<ResultDTO> findAvaliablitly(String city, Date startDate,
			Date endDate, float maxPrice, int numRooms) {

		List<ResultDTO> possibleRooms = new ArrayList<ResultDTO>();
		List<ResultDTO> conflictingBookingRooms = new ArrayList<ResultDTO>();

				
				try{			
					String query2 = "Select hr.Hotel_ID, hr.room_ID from hotel_room hr join room r on hr.room_id = r.id join hotel h on h.id = hr.hotel_id where h.location LIKE ?";

					PreparedStatement stmnt2 = connection.prepareStatement(query2);
					System.out.println("Execute Query PR");

					stmnt2.setString(1, '%'+city+ '%');
					
					ResultSet res2 = stmnt2.executeQuery();
					while(res2.next()){
						int room_id = res2.getInt("ROOM_ID");
						int hotel_id = res2.getInt("HOTEL_ID");

						ResultDTO r = new ResultDTO(hotel_id, room_id);
						r.setStartDate(startDate);
						r.setEndDate(endDate);
						
						possibleRooms.add(r);

					}
					res2.getStatement().close();
					res2.close();
					
				}catch(Exception e){
					//System.out.println("Caught Exception");
					e.printStackTrace();
				}
				
				
				//Now we also want to find bookings
				//Select br.hotel_id, br.room_id  from booking_room br join booking b on br.booking_id = b.id where br.hotel_id = 1 and ((?#startdate between b.START_DATE and b.END_DATE) or (?#enddate between b.START_DATE and b.END_DATE) or (b.start_date between ?#startdate and ?#enddate)or (b.end_date between ?#startdate and ?#enddate))
				//and we save it in list<ResultDTO> conflictingBookingRooms
				
				
				try{			
					String query3 = "Select br.hotel_id, br.room_id  from booking_room br "
							+ "join booking b on br.booking_id = b.id "
							+ "join hotel h on br.hotel_id= h.id "
							+ "where h.location LIKE ? "
							+ "and (( ? between b.START_DATE and b.END_DATE) "
							+ "or ( ? between b.START_DATE and b.END_DATE) "
							+ "or (b.START_DATE between ? and ?) "
							+ "or (b.END_DATE between ? and ?))";

					PreparedStatement stmnt3 = connection.prepareStatement(query3);
					System.out.println("Execute Query CP");

					stmnt3.setString(1, '%'+city+ '%');
					
					stmnt3.setDate(2, new java.sql.Date(startDate.getTime()));
					stmnt3.setDate(3, new java.sql.Date(endDate.getTime()));
					
					stmnt3.setDate(4, new java.sql.Date(startDate.getTime()));
					stmnt3.setDate(5, new java.sql.Date(endDate.getTime()));
					
					stmnt3.setDate(6, new java.sql.Date(startDate.getTime()));
					stmnt3.setDate(7, new java.sql.Date(endDate.getTime()));
					
					ResultSet res3 = stmnt3.executeQuery();
					while(res3.next()){
						int room_id1 = res3.getInt("ROOM_ID");
						int hotel_id1 = res3.getInt("HOTEL_ID");
						
						ResultDTO r = new ResultDTO(hotel_id1, room_id1);
						r.setStartDate(startDate);
						r.setEndDate(endDate);
						
						conflictingBookingRooms.add(r);

					}
					res3.getStatement().close();
					res3.close();
					
				}catch(Exception e){
					//System.out.println("Caught Exception");
					e.printStackTrace();
				}
				
				
				

		
		
		//Now we want to do possible rooms - conflictingBookingRooms
		//Save it in possibleRooms again
		possibleRooms = ResultDTO.subtractResultLists(possibleRooms, conflictingBookingRooms);
		
		//Then we want to populate possiblerooms, 
		//ie add room and hotel to resultDTO
		//Then wowrk out price for each resultDTO  
		//then return.
		possibleRooms = populateResult(possibleRooms);
		
		//Filter to check maxprice
		possibleRooms = ResultDTO.filterPrice(possibleRooms, maxPrice);

		//Only show hotelIds with count >= numRooms
		possibleRooms = ResultDTO.filterCountForHotel(possibleRooms, numRooms);

		
		return possibleRooms;
	}

	private List<ResultDTO> populateResult(List<ResultDTO> possibleRooms) {
		HashMap<Integer, HotelDTO> hotels = new HashMap<Integer, HotelDTO>();
		HashMap<Integer, RoomDTO> rooms = new HashMap<Integer, RoomDTO>();


		for(ResultDTO r: possibleRooms) {
			
			try {
				if(!hotels.containsKey(r.getHotelID())) {
					hotels.put(r.getHotelID(), findHotel(r.getHotelID()));
				}
				r.setHotel(hotels.get(r.getHotelID()));
				if(!rooms.containsKey(r.getRoomID())) {
					rooms.put(r.getRoomID(), findRoom(r.getRoomID()));
				}
				r.setRoom(rooms.get(r.getRoomID()));
			} catch (EmptyResultException e) {
				e.printStackTrace();
			}
			
			r.setMaxNightPrice(0);
			r.setTotalPrice(0);
			
			Date tempDate = r.getStartDate();
			Date endDate = r.getEndDate();
			
			List<DiscountDTO> applicableDiscounts = new ArrayList<DiscountDTO>();
			/*

			//TODO should filter with dates aswell
			try {
				String query = "SELECT * FROM DISCOUNT WHERE HOTEL_ID = ? AND ROOM_ID = ?";
				PreparedStatement stmnt1 = connection.prepareStatement(query);
				stmnt1.setLong(1, r.getHotelID());
				stmnt1.setLong(2, r.getRoomID());
				ResultSet res1 = stmnt1.executeQuery();
				System.out.println("Execute Query dd");

				//logger.info("The result set size is "+res1.getFetchSize());
				while(res1.next()){
					int id1 = res1.getInt("ID");
					float amount = res1.getFloat("AMOUNT");
					Date startDate1 = res1.getDate("START_DATE");
					Date endDate1 = res1.getDate("END_DATE");

					DiscountDTO d = new DiscountDTO(id1, amount, startDate1, endDate1);
					
					applicableDiscounts.add(d);				
				}
				res1.getStatement().close();
				res1.close();
			}catch(Exception e){
				//System.out.println("Caught Exception");
				e.printStackTrace();
			}
			*/
			
			
			while (tempDate.before(endDate)) {
				//get price for that date
				double price = getRoomPrice(r, tempDate, applicableDiscounts);
				
				
				//check if its > maxprice
				if(price > r.getMaxNightPrice()) {
					//set max price
					r.setMaxNightPrice(price);
				}
				//add to total amount
				r.setTotalPrice(r.getTotalPrice() + price);
				
				//add to date1 to date
				tempDate = new Date(tempDate.getTime() + (1000 * 60 * 60 * 24));
			}
			

			
		}
				
		return possibleRooms;
	}

	private double getRoomPrice(ResultDTO r, Date tempDate, List<DiscountDTO> applicableDiscounts) {
		double price = r.getRoom().getPrice();
		
		//then check to see if we need to apply peak rate
		if(Magic.isPeak(tempDate)) {
			price *= 1.4;
		}
	
		//Now loop through these applicant discounts and see if we need to apply discount
		for(DiscountDTO d: applicableDiscounts) {
			if(tempDate.after(d.getStartDate()) && tempDate.before(d.getEndDate())) {
				price *= (1 - d.getAmount()/100);
			}
		}	
		return price;
	}
	
	
	


	@Override
	public BookingDTO getBooking(int ID) throws EmptyResultException {
		
		BookingDTO booking = null;
		
		//GET THE BOOKING INFO
		try{
			//logger.info("The value passed is: "+ ID);
			
			
			String query_cast = "SELECT * FROM BOOKING WHERE ID = ?";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setInt(1, ID);
			System.out.println("Execute Query get booking");
			ResultSet res = stmnt.executeQuery();
			res.next();
			int id = res.getInt("ID");
			String name = res.getString("NAME");
			String email = res.getString("EMAIL");
			String pin = res.getString("PIN");
			int price = res.getInt("PRICE");
			//Java.util.Date newDate = result.getTimestamp("VALUEDATE");
			
			//java.sql.Date startDate = res.getDate("START_DATE");
			//java.sql.Date endDate = res.getDate("END_DATE");
			
			Date startDate = res.getDate("START_DATE");
			Date endDate = res.getDate("END_DATE");

			booking = new BookingDTO(id, name, email, pin, startDate, endDate, price);
			res.getStatement().close();
			res.close();
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
			throw new EmptyResultException();
		}
		
		ArrayList<BookingRoomDTO> bookingRooms = new ArrayList<BookingRoomDTO>();
		//logger.info("meow");
		
		try {			
			String brquery = 
					"SELECT * From BOOKING_ROOM WHERE BOOKING_ID = ?";
			PreparedStatement brstmnt = connection.prepareStatement(brquery);
			brstmnt.setLong(1, ID);
			ResultSet resbr = brstmnt.executeQuery();
			System.out.println("Execute Query booking rooms");
			
			HashMap<Integer, HotelDTO> hotels = new HashMap<Integer, HotelDTO>();
			HashMap<Integer, RoomDTO> rooms = new HashMap<Integer, RoomDTO>();
			
			while(resbr.next()){
				int brid = resbr.getInt("ID");
				int hotelID = resbr.getInt("HOTEL_ID");
				int roomID = resbr.getInt("ROOM_ID");
				int hotelRoomID = resbr.getInt("HOTEL_ROOM_ID");
				boolean extraBed = resbr.getBoolean("EXTRA_BED");

				BookingRoomDTO br = new BookingRoomDTO(brid, extraBed);

				if(!hotels.containsKey(hotelID)) {
					hotels.put(hotelID, findHotel(hotelID));
				}
				br.setHotel(hotels.get(hotelID));
				if(!rooms.containsKey(roomID)) {
					rooms.put(roomID, findRoom(roomID));
					
				}
				br.setRoom(rooms.get(roomID));

				if(hotelRoomID!=0) {
					br.setHotelRoom(findHotelRoom(hotelRoomID));
				}
				
				//logger.info("hotelID: "+hotelID + " roomID "+roomID);
				
				br.setBooking(booking);
				
				bookingRooms.add(br);
			} 
			resbr.getStatement().close();
			resbr.close();
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		
		booking.setRoomBookings(bookingRooms);
		
		
		return booking;
	}

	@Override
	public HotelRoomDTO findHotelRoom(int id) throws EmptyResultException {
		HotelRoomDTO hr = null;
		try{
			//logger.info("The value passed is: "+id);
			
			
			String query_cast = "SELECT * FROM HOTEL_ROOM WHERE ID = ?";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setInt(1, id);
			ResultSet res = stmnt.executeQuery();
			System.out.println("Execute Query fhr");

			res.next();
			String roomNumber = res.getString("ROOM_NUMBER");
			String status = res.getString("STATUS");

			int rooomID = res.getInt("ROOM_ID");
			int HotelID = res.getInt("HOTEL_ID");
			
			//logger.info("cat is" + id+" "+roomNumber+" "+status);
			hr = new HotelRoomDTO(id, roomNumber, status);
			
			hr.setHotel(findHotel(HotelID));
			hr.setRoom(findRoom(rooomID));
			res.getStatement().close();
			res.close();
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
			throw new EmptyResultException();
		}
		
		return hr;
	}


	@Override
	public List<BookingDTO> getBookingsFromHotel(int HotelID) {
		ArrayList<BookingDTO> bookings = new ArrayList<BookingDTO>();
		
		try {
			String query_cast = 
					"SELECT DISTINCT b.ID, b.name, b.email, b.pin, b.start_date, b.end_date, b.price "
					+ "FROM BOOKING b "
					+ "JOIN BOOKING_ROOM br on br.BOOKING_ID = b.ID "
					+ "JOIN HOTEL h on br.HOTEL_ID = h.ID  "
					+ "WHERE h.ID = ?";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setLong(1, HotelID);
			ResultSet res = stmnt.executeQuery();
			while(res.next()){
				int id = res.getInt("ID");
				String name = res.getString("NAME");
				String email = res.getString("EMAIL");
				String pin = res.getString("PIN");
				Date start = res.getDate("START_DATE");
				Date end = res.getDate("END_DATE");
				int price = res.getInt("PRICE");

				BookingDTO b = new BookingDTO(id, name, email, pin, start, end, price);
				//Now for each get the booking room
				
				ArrayList<BookingRoomDTO> bookingRooms = new ArrayList<BookingRoomDTO>();
				
				try {
					String brquery = 
							"SELECT * From BOOKING_ROOM WHERE BOOKING_ID = ?";
					PreparedStatement brstmnt = connection.prepareStatement(brquery);
					brstmnt.setLong(1, id);
					ResultSet resbr = brstmnt.executeQuery();
					System.out.println("Execute Query");

					while(resbr.next()){
						int brid = resbr.getInt("ID");
						int hotelID = resbr.getInt("HOTEL_ID");
						int roomID = resbr.getInt("ROOM_ID");
						int hotelRoomID = resbr.getInt("HOTEL_ROOM_ID");
						boolean extraBed = resbr.getBoolean("EXTRA_BED");

						BookingRoomDTO br = new BookingRoomDTO(brid, extraBed);
						br.setHotel(findHotel(hotelID));
						br.setRoom(findRoom(roomID));
						if(hotelRoomID!=0) {
							br.setHotelRoom(findHotelRoom(hotelRoomID));
						}
						
						br.setBooking(b);
						
						bookingRooms.add(br);
					} 
					resbr.getStatement().close();
					resbr.close();
				}catch(Exception e){
					//System.out.println("Caught Exception");
					e.printStackTrace();
				}
				
				b.setRoomBookings(bookingRooms);
				bookings.add(b);
			} 
			res.getStatement().close();
			res.close();
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		
		return bookings;
	}
	
	
	@Override
	public int newBooking(BookingDTO booking) {
		PreparedStatement stmnt = null; 
		int id = 0; 
		
		try{
			String sqlStr = 
				"INSERT INTO BOOKING (NAME, EMAIL, PIN, START_DATE, END_DATE, PRICE) "+
				"VALUES (?,?,?,?, ?,?)";
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setString(1, booking.getName());
			stmnt.setString(2, booking.getEmail());
			stmnt.setString(3, booking.getPin());
			stmnt.setDate(4,new java.sql.Date(booking.getStartDate().getTime()));
			stmnt.setDate(5,new java.sql.Date(booking.getEndDate().getTime()));
			stmnt.setInt(6, booking.getPrice());
			
			//logger.info("sql string is "+stmnt.toString());
			int result = stmnt.executeUpdate();
			System.out.println("Execute Query");

			//logger.info("Statement successfully executed "+result);
			
			try {
				String q1 = "Select max(id) from booking";
				PreparedStatement s1 = connection.prepareStatement(q1);
				ResultSet res = s1.executeQuery();
				System.out.println("Execute Query");

				res.next();
				id = res.getInt(1);
				res.getStatement().close();
				res.close();
			}catch(Exception e){
				//System.out.println("Caught Exception");
				e.printStackTrace();
			}
			
			
			for (BookingRoomDTO br : booking.getRoomBookings()) {
				sqlStr = 
						"INSERT INTO BOOKING_ROOM (BOOKING_ID, HOTEL_ID, ROOM_ID, EXTRA_BED) "+
						"VALUES (?,?,?,?)";
					stmnt = connection.prepareStatement(sqlStr);
					stmnt.setInt(1, id);
					stmnt.setInt(2, br.getHotel().getID());
					stmnt.setInt(3, br.getRoom().getID());
					stmnt.setBoolean(4, br.isExtraBed());
					
					//logger.info("sql string is "+stmnt.toString());
					result = stmnt.executeUpdate();
					System.out.println("Execute Query");

					//logger.info("Statement successfully executed "+result);
			}

			stmnt.close();
			
			return id;
			
		}catch(Exception e){
			//logger.severe("Unable to store hotel! ");
			e.printStackTrace();
		}
		
		
		return 0;
		
		
	}

	@Override
	public void updateBooking(List<ResultDTO> booking, int id, int price) {
		PreparedStatement stmnt = null; 
		
		try{
			
			for (ResultDTO br : booking) {
				String sqlStr = 
						"INSERT INTO BOOKING_ROOM (BOOKING_ID, HOTEL_ID, ROOM_ID, EXTRA_BED) "+
						"VALUES (?,?,?,?)";
					stmnt = connection.prepareStatement(sqlStr);
					stmnt.setInt(1, id);
					stmnt.setInt(2, br.getHotel().getID());
					stmnt.setInt(3, br.getRoom().getID());
					stmnt.setBoolean(4, br.isExtraBed());
					
					//logger.info("sql string is "+stmnt.toString());
					int result = stmnt.executeUpdate();
					System.out.println("Execute Query");

					//logger.info("Statement successfully executed "+result);
			}

			String sqlStr = 
					"UPDATE Booking "
					+ "SET price=?"
					+ "WHERE id=?";
			
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setInt(1, price);
			stmnt.setInt(2, id);

			int result = stmnt.executeUpdate();
			System.out.println("Execute Query");

			//logger.info("Statement successfully executed "+result);
			
			stmnt.close();	
		}catch(Exception e){
			//logger.severe("Unable to store hotel! ");
			e.printStackTrace();
		}
	}

	@Override
	public void addRoomToHotel(HotelRoomDTO HotelRoom) {
		PreparedStatement stmnt = null; 
		
		try{
			String sqlStr = 
				"INSERT INTO HOTEL_ROOM (ROOM_NUMBER, STATUS, HOTEL_ID, ROOM_ID) "+
				"VALUES (?,?,?,?)";
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setString(1, HotelRoom.getRoomNumber());
			stmnt.setString(2, HotelRoom.getStatus());
			stmnt.setInt(3,  HotelRoom.getHotel().getID());
			stmnt.setInt(4,  HotelRoom.getRoom().getID());

			//logger.info("sql string is "+stmnt.toString());
			int result = stmnt.executeUpdate();
			System.out.println("Execute Query");

			//logger.info("Statement successfully executed "+result);
			stmnt.close();
		}catch(Exception e){
			//logger.severe("Unable to store hotel room! ");
			e.printStackTrace();
		}
		
	}

	@Override
	public List<HotelRoomDTO> getRoomsFromHotel(int HotelID) {
		ArrayList<HotelRoomDTO> hotelRooms = new ArrayList<HotelRoomDTO>();
		
		try {
			String query_cast = "SELECT * FROM HOTEL_ROOM WHERE HOTEL_ID = ?";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setLong(1, HotelID);
			ResultSet res = stmnt.executeQuery();
			System.out.println("Execute Query");

			//logger.info("The result set size is "+res.getFetchSize());
			while(res.next()){
				int id = res.getInt("ID");
				String roomNumber = res.getString("ROOM_NUMBER");
				String status = res.getString("STATUS");
				int rooomID = res.getInt("ROOM_ID");

				//logger.info(id+" "+roomNumber+" "+status);
				HotelRoomDTO hr = new HotelRoomDTO(id, roomNumber, status);
				
				hr.setHotel(findHotel(HotelID));
				hr.setRoom(findRoom(rooomID));
				
				hotelRooms.add(hr);				
			}
			res.getStatement().close();
			res.close();
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		
		return hotelRooms;
	}

	@Override
	public RoomDTO findRoom(int roomID) throws EmptyResultException {
		RoomDTO room = null;
		try{
			//logger.info("The value passed is: "+roomID);
			
			
			String query_cast = "SELECT * FROM ROOM WHERE ID = ?";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setInt(1, roomID);
			ResultSet res = stmnt.executeQuery();
			System.out.println("Execute Query find room");

			res.next();
			int id = res.getInt("id");
			String type = res.getString("TYPE");
			float price = res.getFloat("PRICE");
			boolean extraBedAllowed = res.getBoolean("EXTRA_BED_ALLOWED");

			//logger.info(id+" "+type+" "+price);

			room = new RoomDTO(id, type, price, extraBedAllowed);
			res.getStatement().close();
			res.close();
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
			throw new EmptyResultException();
		}
		
		return room;
	}


	@Override
	public List<HotelDTO> findAll() {
		ArrayList<HotelDTO> hotels = new ArrayList<HotelDTO>();
		try{
			Statement stmnt = connection.createStatement();
			String query_cast = "SELECT ID, NAME, LOCATION FROM HOTEL";
			ResultSet res = stmnt.executeQuery(query_cast);
			System.out.println("Execute Query");

			//logger.info("The result set size is "+res.getFetchSize());
			while(res.next()){
				int id = res.getInt("ID");
				String name = res.getString("NAME");
				String location = res.getString("LOCATION");
				//logger.info(id+" "+name+" "+location);
				HotelDTO h = new HotelDTO(id, name, location);
				h.setHotelRooms(getRoomsFromHotel(id));
				hotels.add(h);				
				
			}
			
			res.close();
			stmnt.close();
			
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		return hotels;
	}
	
	@Override
	public List<RoomDTO> getRoomTypes() {
		ArrayList<RoomDTO> room_types = new ArrayList<RoomDTO>();
		try{
			Statement stmnt = connection.createStatement();
			String query_cast = "SELECT * FROM ROOM";
			ResultSet res = stmnt.executeQuery(query_cast);
			System.out.println("Execute Query");

			//logger.info("The result set size is "+res.getFetchSize());
			while(res.next()){
				int id = res.getInt("ID");
				String type = res.getString("TYPE");
				float price = res.getFloat("PRICE");
				boolean extraBedAllowed = res.getBoolean("EXTRA_BED_ALLOWED");
				//logger.info(id+" "+type+" "+price);
				RoomDTO r = new RoomDTO(id, type, price, extraBedAllowed);
				room_types.add(r);				
				
			}
			
			res.close();
			stmnt.close();
			
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		return room_types;
	}
	
	@Override
	public boolean hotelRoomBooked(int hotelRoomID, Date date ) {
		boolean booked = false;
		try {
			String query_cast = 
					"SELECT * FROM BOOKING b "
					+ " JOIN BOOKING_ROOM br on br.BOOKING_ID = b.ID "
					+ " WHERE br.HOTEL_ROOM_ID = ? AND ( ? between b.START_DATE and b.END_DATE)";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			//logger.info("HOTERLROOMID " + hotelRoomID);
			//logger.info("DATE " + new java.sql.Date(date.getTime()));
			stmnt.setLong(1, hotelRoomID);
			stmnt.setDate(2, new java.sql.Date(date.getTime()));
			
			ResultSet res = stmnt.executeQuery();
			System.out.println("Execute Query");

			booked = res.next();
			res.close();
			stmnt.close();
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		
		return booked;
		
	}

	
	@Override
	public Map<String, Integer> roomTypeCountHotel(int ID) {

		Map<String,  Integer> counts = new HashMap<String, Integer>();
		
		try{
			Statement stmnt = connection.createStatement();
			String query_cast = "SELECT TYPE FROM ROOM";
			ResultSet res = stmnt.executeQuery(query_cast);
			System.out.println("Execute Query");

			while(res.next()){
				String type = res.getString("TYPE");

				
				try {
					String subquery = 
							"Select count(*) from hotel_room hr "
							+ "join room r on hr.room_id = r.id "
							+ "where hr.hotel_id = ? and r.type = ?";
		
					PreparedStatement substmnt = connection.prepareStatement(subquery);
					substmnt.setLong(1, ID);
					substmnt.setString(2, type);
					
					ResultSet subres = substmnt.executeQuery();
					subres.next();
					
					counts.put(type, subres.getInt(1));
										
					subres.close();
					substmnt.close();
				}catch(Exception e){
					//System.out.println("Caught Exception");
					e.printStackTrace();
				}
				
				
			}
			
			res.close();
			stmnt.close();
			
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		
		return counts;
		
	}

	@Override
	public void updateRoomStatus(int hotelRoomID, String status) {
		PreparedStatement stmnt = null;
		//logger.info("status is" +status);
		try{
			String sqlStr = 
				"UPDATE hotel_room "
				+ "SET status=?"
				+ "WHERE id=?";
			
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setString(1, status);
			stmnt.setInt(2, hotelRoomID);

			int result = stmnt.executeUpdate();
			System.out.println("Execute Query");

			//logger.info("Statement successfully executed "+result);
			stmnt.close();
		}catch(Exception e){
			//logger.severe("Unable to store comment! ");
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void checkInRoom(int roomBookingID, int hotelRoomID) {
		PreparedStatement stmnt = null; 
		
		try{
			String sqlStr = 
				"UPDATE booking_room "
				+ "SET hotel_room_id=?"
				+ "WHERE id=?";
			
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setInt(1, hotelRoomID);
			stmnt.setInt(2, roomBookingID);

			int result = stmnt.executeUpdate();
			System.out.println("Execute Query");

			//logger.info("Statement successfully executed "+result);
			stmnt.close();
		}catch(Exception e){
			//logger.severe("Unable to store comment! ");
			e.printStackTrace();
		}
		
		try{
			String sqlStr = 
				"UPDATE hotel_room "
				+ "SET status='Occupied'"
				+ "WHERE id=?";
			
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setInt(1, hotelRoomID);

			int result = stmnt.executeUpdate();
			System.out.println("Execute Query");

			//logger.info("Statement successfully executed "+result);
			stmnt.close();
		}catch(Exception e){
			//logger.severe("Unable to store comment! ");
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void checkOutRoom(int hotelRoomID) {
		PreparedStatement stmnt = null; 
		
		try{
			String sqlStr = 
				"UPDATE hotel_room "
				+ "SET status='Avaliable'"
				+ "WHERE id=?";
			
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setInt(1, hotelRoomID);

			int result = stmnt.executeUpdate();
			System.out.println("Execute Query");

			//logger.info("Statement successfully executed "+result);
			stmnt.close();
		}catch(Exception e){
			//logger.severe("Unable to store comment! ");
			e.printStackTrace();
		}
		
	}


	@Override
	public DiscountDTO getDiscount(int id) throws EmptyResultException {
		DiscountDTO discount = null;
		try{			
			
			String query_cast = "SELECT * FROM DISCOUNT WHERE ID = ?";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setInt(1, id);
			ResultSet res = stmnt.executeQuery();
			System.out.println("Execute Query");

			res.next();
			float amount = res.getFloat("AMOUNT");
			Date start_date = res.getDate("START_DATE");
			Date end_date = res.getDate("END_DATE");
			//set room and hotel
			discount = new DiscountDTO(id, amount, start_date, end_date);
			
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
			throw new EmptyResultException();
		}
		
		return discount;
	}
	
	@Override
	public void newDiscount(DiscountDTO discount) {
		PreparedStatement stmnt = null; 
		
		try{
			String sqlStr = 
				"INSERT INTO DISCOUNT (HOTEL_ID,ROOM_ID,AMOUNT,START_DATE,END_DATE) "+
				"VALUES (?,?,?,?, ?)";
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setInt(1, discount.getHotel().getID());
			stmnt.setInt(2, discount.getRoom().getID());
			stmnt.setFloat(3,  discount.getAmount());
			stmnt.setDate(4, new java.sql.Date(discount.getStartDate().getTime()));
			stmnt.setDate(5,  new java.sql.Date(discount.getEndDate().getTime()));

			//logger.info("sql string is "+stmnt.toString());
			int result = stmnt.executeUpdate();
			System.out.println("Execute Query");

			//logger.info("Statement successfully executed "+result);
			stmnt.close();
		}catch(Exception e){
			//logger.severe("Unable to store discount room! ");
			e.printStackTrace();
		}		
	}


	@Override
	public List<BookingDTO> getCurrentBookingsForHotel(int hotelID) {
		ArrayList<BookingDTO> bookings = new ArrayList<BookingDTO>();
		
		try {
			String query_cast = 
					"SELECT DISTINCT b.ID, b.name, b.email, b.pin, b.start_date, b.end_date, b.price  "
					+ "FROM BOOKING b "
					+ "JOIN BOOKING_ROOM br on br.BOOKING_ID = b.ID "
					+ "JOIN HOTEL h on br.HOTEL_ID = h.ID  "
					+ "WHERE h.ID = ?  AND b.END_DATE >= CURRENT_DATE AND b.start_date <= CURRENT_DATE";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setLong(1, hotelID);
			ResultSet res = stmnt.executeQuery();
			System.out.println("Execute Query");

			while(res.next()){
				int id = res.getInt("ID");
				String name = res.getString("NAME");
				String email = res.getString("EMAIL");
				String pin = res.getString("PIN");
				Date start = res.getDate("START_DATE");
				Date end = res.getDate("END_DATE");
				int price = res.getInt("PRICE");

				BookingDTO b = new BookingDTO(id, name, email, pin, start, end, price);
				//Now for each get the booking room
				
				ArrayList<BookingRoomDTO> bookingRooms = new ArrayList<BookingRoomDTO>();
				
				try {
					String brquery = 
							"SELECT * From BOOKING_ROOM WHERE BOOKING_ID = ?";
					PreparedStatement brstmnt = connection.prepareStatement(brquery);
					brstmnt.setLong(1, id);
					ResultSet resbr = brstmnt.executeQuery();
					System.out.println("Execute Query");

					
					while(resbr.next()){
						int brid = resbr.getInt("ID");
						int HotelID = resbr.getInt("HOTEL_ID");
						int roomID = resbr.getInt("ROOM_ID");
						int hotelRoomID = resbr.getInt("HOTEL_ROOM_ID");
						boolean extraBed = resbr.getBoolean("EXTRA_BED");

						BookingRoomDTO br = new BookingRoomDTO(brid, extraBed);
						br.setHotel(findHotel(HotelID));
						br.setRoom(findRoom(roomID));
						if(hotelRoomID!=0) {
							br.setHotelRoom(findHotelRoom(hotelRoomID));
						}
						
						br.setBooking(b);
						
						bookingRooms.add(br);
					} 
					resbr.getStatement().close();
					resbr.close();
				}catch(Exception e){
					//System.out.println("Caught Exception");
					e.printStackTrace();
				}
				
				b.setRoomBookings(bookingRooms);
				bookings.add(b);
			} 
			res.getStatement().close();
			res.close();
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		
		return bookings;
	}


	@Override
	public boolean canAddRoomToHotel(int roomID, int hotelID) {
		int count = 0;
		int maxAllowed = 0;
		try {
			String query_cast = "select count(*) from HOTEL_ROOM hr  "
				+ " where hr.HOTEL_ID = ? and "
				+ " hr.ROOM_ID = ?";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setLong(1, hotelID);
			stmnt.setLong(2, roomID);
			ResultSet res = stmnt.executeQuery();
			System.out.println("Execute Query");

			res.next();
			count = res.getInt(1);
			
			query_cast = "select max_allowed from ROOM where id = ?";
			stmnt = connection.prepareStatement(query_cast);
			stmnt.setLong(1, roomID);
			res = stmnt.executeQuery();
			System.out.println("Execute Query");

			res.next();
			maxAllowed = res.getInt("MAX_ALLOWED");

			
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		
		return count < maxAllowed;
	}


	@Override
	public boolean canEditBooking(int hotelID) {
		Date startDate = null;
		
		try{			
			
			String query_cast = "SELECT * FROM BOOKING WHERE ID = ?";
			PreparedStatement stmnt = connection.prepareStatement(query_cast);
			stmnt.setInt(1, hotelID);
			ResultSet res = stmnt.executeQuery();
			System.out.println("Execute Query");

			res.next();
			startDate = res.getDate("START_DATE");

			res.getStatement().close();
			res.close();
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		
		
		Date today = new Date();
		Date before48 = new Date(today.getTime() - (1000 * 60 * 60 * 24 * 2));
		
		if(startDate.getTime() >= before48.getTime() ) {
			return true;
		}
		
		return false;
	}


	@Override
	public boolean goodToken(String token) {
		int count = 0;
		
		try{			
					
		String query_cast = "SELECT count(*) FROM form_tokens WHERE token = ?";
		PreparedStatement stmnt = connection.prepareStatement(query_cast);
		stmnt.setString(1, token);
		ResultSet res = stmnt.executeQuery();
		System.out.println("Execute Query");

		res.next();
		count = res.getInt(1);
		
			
		}catch(Exception e){
			//System.out.println("Caught Exception");
			e.printStackTrace();
		}
		
		if (count == 1) {
			try{			
				
				String query_cast = "DELETE FROM form_tokens WHERE token = ?";
				PreparedStatement stmnt = connection.prepareStatement(query_cast);
				stmnt.setString(1, token);
				stmnt.execute();
				System.out.println("Execute Query");

				
			}catch(Exception e){
				//System.out.println("Caught Exception");
				e.printStackTrace();
			}
			
			
			return true;
		} else {
			return true; //Change this
		}
		
	}


	@Override
	public String newFormToken(String prefix) {
		String token = prefix + UUID.randomUUID().toString();
		
		PreparedStatement stmnt = null; 
		
		try{
			String sqlStr = 
				"INSERT INTO FORM_TOKENS (token) "+
				"VALUES (?)";
			stmnt = connection.prepareStatement(sqlStr);
			stmnt.setString(1, token);
			int result = stmnt.executeUpdate();
			System.out.println("Execute Query");

			stmnt.close();
		}catch(Exception e){
			//logger.severe("Unable to store token! ");
			e.printStackTrace();
		}
		
		
		
		return token;
	}

	
	
}
