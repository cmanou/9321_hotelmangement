package com.groupN.jdbc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


public class HotelDTO {
	 
	private String location;
	private String name;
	private int ID;
	private List<HotelRoomDTO> hotelRooms;
	private List<DiscountDTO> discounts;

	public HotelDTO(int ID, String name, String location) {
		this.location = location;
		this.name = name;
		this.ID = ID;
	}
	
	public HotelDTO(String name, String location) {
		this.location = location;
		this.name = name;
	}

	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getID() {
		return ID;
	}
	
	public void setID(int ID) {
		this.ID = ID;
	}
	
	public List<HotelRoomDTO> getHotelRooms() {
		return hotelRooms;
	}
	 
	public void setHotelRooms(List<HotelRoomDTO> hotelRooms) {
		this.hotelRooms = hotelRooms;
	}
	
	
	public int getAvaliable() {
		int count = 0;
		for (HotelRoomDTO hr: hotelRooms) {
			if(!hr.bookedToday()) {
				count++;;
			}
		}
		return count;
	}
	
	public int getBooked() {
		int count = 0;
		for (HotelRoomDTO hr: hotelRooms) {
			if(hr.bookedToday()) {
				count++;;
			}
		}
		return count;
	}

	public List<HotelRoomDTO> avaliableRooms(String type) {
		List<HotelRoomDTO> avaliable = new ArrayList<HotelRoomDTO>();
		System.out.println("ROOM COUNT: " + hotelRooms.size());
		for(HotelRoomDTO hr: hotelRooms) {
			System.out.println("Status: " + hr.getStatus() + "Type" + hr.getRoom().getType());
			if(hr.getStatus().equals("Available") && hr.getRoom().getType().equals(type)) {
				avaliable.add(hr);
				System.out.println("Added Room");
			}
		}
		return avaliable;
	}

	public List<DiscountDTO> getDiscounts() {
		return discounts;
	}
	
	public List<DiscountDTO> getCurrentDiscounts() {
		List<DiscountDTO> currentDiscounts = new ArrayList<DiscountDTO>();
		
		Date today = new Date();
		
		for(DiscountDTO d: discounts) {
			System.out.println(d.getEndDate().getTime() + ":" + today.getTime());
			System.out.println(d.getEndDate().getTime() >= today.getTime());
			if(d.getEndDate().getTime() >= today.getTime() ) {
				currentDiscounts.add(d);
			}
		}	
		
		return currentDiscounts;
	}

	public void setDiscounts(List<DiscountDTO> discounts) {
		this.discounts = discounts;
	}
	
	public List<HotelRoomDTO> getUnderMaintenance() {
		List<HotelRoomDTO> retVal = new ArrayList<HotelRoomDTO>();
		
		for (HotelRoomDTO hr : hotelRooms) {
			if (hr.getStatus().equals("Under maintenance")) {
				retVal.add(hr);
			}
		}
		
		return retVal;
	}
	
	public List<HotelRoomDTO> getNotUnderMaintenance() {
		List<HotelRoomDTO> retVal = new ArrayList<HotelRoomDTO>();
		
		for (HotelRoomDTO hr : hotelRooms) {
			if (!hr.getStatus().equals("Under maintenance")) {
				retVal.add(hr);
			}
		}
		
		return retVal;
	}
	
	public double getOccupancy() {
		double occupancy = 0;
		int numRooms = hotelRooms.size();
		int available = getAvaliable();
		
		occupancy = numRooms-available;
		if (occupancy != 0)
			occupancy  = Math.ceil((occupancy / numRooms)*100);
		
		return occupancy;
	}
		
	
	

}
