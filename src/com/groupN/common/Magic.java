package com.groupN.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class Magic {
    private static SimpleDateFormat sdfrmt = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

    
	public static boolean validDate(String strDate)
	{
	    if (strDate.isEmpty())
	    {
	        return false;
	    }
	    else
	    {
	        sdfrmt.setLenient(false);
	        Date javaDate = null;
	        try
	        {
	            javaDate = sdfrmt.parse(strDate); 
//	            System.out.println("Date after validation: " + javaDate);
	        }
	        catch (ParseException e)
	        {
//	            System.out.println("The date you provided is in an " +"invalid date format.");
	            return false;
	        }
	        return true;
	    }
	} 
	
	public static Date parseDate(String strDate) {
		Date date = null;
		
		try {
			date = sdfrmt.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
	}
	
	public static boolean validInteger(String s) {
		 if (s.isEmpty())
		    {
		        return false;
		    }
		else
		    {
		    try { 
		        Integer.parseInt(s); 
		    } catch(NumberFormatException e) { 
		        return false; 
		    }
		    return true;
		}
	}

	public static boolean validFloat(String s) {
		 if (s.isEmpty())
		    {
		        return false;
		    }
		else
		    {
		    try { 
		        Float.parseFloat(s); 
		    } catch(NumberFormatException e) { 
		        return false; 
		    }
		    return true;
		}
	}

	public static boolean validString(String s) {
		if (s.isEmpty())
	    {
	        return false;
	    }
		return true;
	}
	
	public static boolean validEmail(String s) {
		if (s.isEmpty()) {
	        return false;
	    } else {
	    	boolean result = true;
	    	try {
	    		InternetAddress emailAddr = new InternetAddress(s);
	    		emailAddr.validate();
	    	} catch (AddressException ex) {
	    		result = false;
	    	}
	    	return result;
	    }
	}

	public static boolean isPeak(Date date) {
		Calendar day = new GregorianCalendar();
		day.setTime(date);
	
		//Peak periods are 
		//TODO optimize or fix this
		//Dec. 15th - Feb 15th, 
		if ((day.get(Calendar.MONTH) == 11 
				&& day.get(Calendar.DAY_OF_MONTH) >= 15)
				|| (day.get(Calendar.MONTH) == 0)
				|| (day.get(Calendar.MONTH) == 1
				&& day.get(Calendar.DAY_OF_MONTH) <= 15)) {
			return true;
		}
		//March 25th - April 14th, 
		else if ((day.get(Calendar.MONTH) == 2 
				&& day.get(Calendar.DAY_OF_MONTH) >= 25) 
				|| (day.get(Calendar.MONTH) == 3
				&& day.get(Calendar.DAY_OF_MONTH) <= 14)) {
			return true;
		}
			
		//July 1st - July 20th 
		else if(day.get(Calendar.MONTH) == 6 
			&& day.get(Calendar.DAY_OF_MONTH) >= 1
			&& day.get(Calendar.DAY_OF_MONTH) <= 20) {
			return true;
		}
		//Sept. 20th - Oct 10th
		else if ((day.get(Calendar.MONTH) == 8 
				&& day.get(Calendar.DAY_OF_MONTH) >= 20) 
				|| (day.get(Calendar.MONTH) == 9
				&& day.get(Calendar.DAY_OF_MONTH) <= 10)) {
			return true;
		}
		
		
		return false;
	}

}
