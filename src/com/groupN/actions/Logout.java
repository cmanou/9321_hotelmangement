package com.groupN.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Logout implements Action {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		
		request.getSession().invalidate();
		
		return "/search.jsp";
	}

}
