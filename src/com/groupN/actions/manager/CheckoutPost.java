package com.groupN.actions.manager;

import java.sql.SQLException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;

public class CheckoutPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(CheckoutPost.class.getName());

	
	public CheckoutPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		String token = request.getParameter("token");
		System.out.println("GOT TOKEN + " + token );

		boolean firstSubmission = hotelChain.goodToken(token);
		
		int bookingID = Integer.parseInt(request.getParameter("id"));

		if (firstSubmission) {
			
			//save the stuff
			int hotelRoomID = Integer.parseInt(request.getParameter("hotel_room"));
	
			hotelChain.checkOutRoom(hotelRoomID);
			
		} 
		

		//redirect to the checkin page with same details
		request.setAttribute("id", bookingID);
		String redirect = "";
		try {
			redirect = (new CheckoutGet()).execute(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		}
		
		return redirect;

		
	}

}
