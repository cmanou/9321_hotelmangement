package com.groupN.actions.manager;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;

public class CheckinPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(CheckinPost.class.getName());

	
	public CheckinPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		String token = request.getParameter("token");
		System.out.println("GOT TOKEN + " + token );

		boolean firstSubmission = hotelChain.goodToken(token);

		int bookingID = Integer.parseInt(request.getParameter("id"));


		if (firstSubmission) {

			//save the stuff
			
			int bookingRoomID = Integer.parseInt(request.getParameter("booking_room"));
			int hotelRoomID = Integer.parseInt(request.getParameter("hotel_room"));
	
			hotelChain.checkInRoom(bookingRoomID, hotelRoomID);
			System.out.print("meow in here");
			
		} 
		
		//redirect to the checkin page with same details
		request.setAttribute("id", bookingID);
		String redirect = "";
		try {
			redirect = (new CheckinGet()).execute(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		}
		
		return redirect;
		
		
	}

}
