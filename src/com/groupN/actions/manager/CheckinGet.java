package com.groupN.actions.manager;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelDTO;

public class CheckinGet implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(CheckinGet.class.getName());

	
	public CheckinGet() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		int bookingID = Integer.parseInt(request.getParameter("id"));
		
		
		String token = hotelChain.newFormToken("checkIn");
		request.setAttribute("token", token);
		
		BookingDTO booking = null;
		try {
			booking = hotelChain.getBooking(bookingID);
		} catch (EmptyResultException e) {
			e.printStackTrace();
		}
		
		logger.info("BLAH" + booking.getRoomBookings().size());
		request.setAttribute("booking", booking);
		return "/WEB-INF/restricted/manager/checkin.jsp";
	}

}
