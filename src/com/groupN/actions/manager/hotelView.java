package com.groupN.actions.manager;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelDTO;

public class hotelView implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(hotelView.class.getName());

	
	public hotelView() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		int hotelID = Integer.parseInt(request.getParameter("id"));
		logger.info(""+ hotelID);
		HotelDTO hotel = null;
		List<BookingDTO> bookings = null;
		try {
			hotel = hotelChain.findHotel(hotelID);
			bookings = hotelChain.getCurrentBookingsForHotel(hotelID);
		} catch (EmptyResultException e) {
			e.printStackTrace();
		}
		request.setAttribute("hotel", hotel);
		request.setAttribute("bookings", bookings);
		return "/WEB-INF/restricted/manager/hotelView.jsp";
	}

}
