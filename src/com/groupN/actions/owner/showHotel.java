package com.groupN.actions.owner;

import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelDTO;
import com.groupN.jdbc.HotelRoomDTO;

public class showHotel implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(newDiscountPost.class.getName());

	
	public showHotel() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		int hotelID = Integer.parseInt(request.getParameter("id"));
		
		logger.info("hotel is"+ hotelID);
		HotelDTO hotel = null;
		Map<String, Integer> roomCounts = null;
		try {
			hotel = hotelChain.findHotel(hotelID);
			roomCounts = hotelChain.roomTypeCountHotel(hotelID);
		} catch (EmptyResultException e) {
			e.printStackTrace();
		}
		request.setAttribute("hotel", hotel);
		request.setAttribute("roomCounts", roomCounts);
		
		
		return "/WEB-INF/restricted/owner/showHotel.jsp";
	}

}

