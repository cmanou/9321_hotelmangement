package com.groupN.actions.owner;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.common.Magic;
import com.groupN.common.ModifiableRequest;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.DiscountDTO;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelRoomDTO;

public class newDiscountPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(newDiscountPost.class.getName());
	
	public newDiscountPost() throws ServletException {
		super();

		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response){
		
		List<String> errors = new ArrayList<String>();
		
		String token = request.getParameter("token");
		System.out.println("GOT TOKEN + " + token );

		boolean firstSubmission = hotelChain.goodToken(token);
		

		if (firstSubmission) {	
			float amount = 0;
			String sDate = request.getParameter("startDate");
			String eDate = request.getParameter("endDate");
			Date startDate = null;
			Date endDate = null;
			int hotelID = 0;
			int roomID = 0;

			
			if(Magic.validFloat(request.getParameter("amount"))) {
				amount = Float.parseFloat(request.getParameter("amount"));
			} else {
				errors.add("Invalid Discount");
			}
			
			if(Magic.validDate(sDate)) {
				startDate = Magic.parseDate(sDate);
			} else {
				errors.add("Invalid Start Date");
			}
			
			if(Magic.validDate(eDate)) {
				endDate = Magic.parseDate(eDate);
			} else {
				errors.add("Invalid End Date");
			}
			
			if(startDate!=null && endDate !=null && 
				startDate.getTime()> endDate.getTime()) {
				errors.add("Invalid Date Range");

			}
			
			if(Magic.validInteger(request.getParameter("hotel"))) {
				hotelID = Integer.parseInt(request.getParameter("hotel"));
			} else {
				errors.add("Invalid Hotel");
			}
			
			if(Magic.validInteger(request.getParameter("room_type"))) {
				roomID = Integer.parseInt(request.getParameter("room_type"));
			} else {
				errors.add("Invalid Room");
			}
			
			if(errors.isEmpty()) {
			
				DiscountDTO d = new DiscountDTO(amount, startDate, endDate);
				
				try {
					d.setHotel(hotelChain.findHotel(hotelID));
					d.setRoom(hotelChain.findRoom(roomID));
				} catch (EmptyResultException e) {
					e.printStackTrace();
				}
				
				 
				hotelChain.newDiscount(d); 
				
				
				//THe amazing 
				Map<String, String[]> extraParams = new TreeMap<String, String[]>();
				extraParams.put("id", new String[]{""+hotelID});
				HttpServletRequest wrappedRequest = new ModifiableRequest(request, extraParams);
				
				try {
					Action redirect = new showHotel();
					return redirect.execute(wrappedRequest, response);
				} catch (ServletException e) {
					e.printStackTrace();
				}
				
			} else {
				request.setAttribute("errors", errors);
				
				try {
					Action redirect = new newDiscountGet();
					return redirect.execute(request, response);
				} catch (ServletException e) {
					e.printStackTrace();
				}
								
			}
			
		}
		
		
		String goHere = "";
		
		//THe amazing 
		Map<String, String[]> extraParams = new TreeMap<String, String[]>();
		extraParams.put("id", new String[]{""+Integer.parseInt(request.getParameter("hotel"))});
		HttpServletRequest wrappedRequest = new ModifiableRequest(request, extraParams);
		
		
		try {
			Action redirect = new showHotel();
			goHere = redirect.execute(wrappedRequest, response);
		} catch (ServletException e) {
			e.printStackTrace();
		}
		return goHere; 
	}
	


}
