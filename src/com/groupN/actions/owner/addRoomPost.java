package com.groupN.actions.owner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.common.Magic;
import com.groupN.common.ModifiableRequest;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelRoomDTO;

public class addRoomPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(addRoomPost.class.getName());

	
	public addRoomPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response){
		
		List<String> errors = new ArrayList<String>();
		
		String token = request.getParameter("token");
		System.out.println("GOT TOKEN + " + token );

		boolean firstSubmission = hotelChain.goodToken(token);
		
		System.out.println("GOT TOKEN status + " + firstSubmission);

		int hotelID = Integer.parseInt(request.getParameter("hotel"));

		if (firstSubmission) {
				
			String roomNumber = "";
			
			if(Magic.validString(request.getParameter("room_number"))) {
				roomNumber = request.getParameter("room_number");
			} else {
				errors.add("Room Number Empty");
			}
			
		
			int roomID = Integer.parseInt(request.getParameter("room_type"));
			
			HotelRoomDTO hr = new HotelRoomDTO(roomNumber, "Available");
			
			boolean canAdd = hotelChain.canAddRoomToHotel(roomID, hotelID);
			
			if(!canAdd) {
				errors.add("You already have to many rooms of the type");
			}
			
			if(!errors.isEmpty()) {
				request.setAttribute("errors", errors);
				
				Action redirect = null;
				try {
					redirect = new addRoomGet();
				} catch (ServletException e) {
					e.printStackTrace();
				}
				return redirect.execute(request, response);
			} else {
				try {
					hr.setHotel(hotelChain.findHotel(hotelID));
					hr.setRoom(hotelChain.findRoom(roomID));
				} catch (EmptyResultException e) {
					e.printStackTrace();
				}
				
				hotelChain.addRoomToHotel(hr);
			}
				
		} 
		
		
		//THe amazing 
		Map<String, String[]> extraParams = new TreeMap<String, String[]>();
		extraParams.put("id", new String[]{""+hotelID});
		HttpServletRequest wrappedRequest = new ModifiableRequest(request, extraParams);
		
		Action redirect=null;
		try {
			redirect = new showHotel();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		return redirect.execute(wrappedRequest, response); 
		
		

	}

}
