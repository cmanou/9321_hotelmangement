package com.groupN.actions.owner;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelDTO;
import com.groupN.jdbc.RoomDTO;

public class addRoomGet implements Action{
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(addRoomGet.class.getName());

	
	public addRoomGet() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		List<HotelDTO> hotels = hotelChain.findAll();
		request.setAttribute("hotels", hotels);
		
		List<RoomDTO> rooms = hotelChain.getRoomTypes();
		request.setAttribute("rooms", rooms);
		
		String token = hotelChain.newFormToken("addRoom");
		System.out.println("Saved TOKEN + " + token);
		request.setAttribute("token", token);
		
		return "/WEB-INF/restricted/owner/addRoom.jsp";
	}

}
