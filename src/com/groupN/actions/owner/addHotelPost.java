package com.groupN.actions.owner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.common.Magic;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelDTO;

public class addHotelPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(addHotelGet.class.getName());

	
	public addHotelPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		
		List<String> errors = new ArrayList<String>();
		
		String token = request.getParameter("token");
		System.out.println("GOT TOKEN + " + token );

		boolean firstSubmission = hotelChain.goodToken(token);
		

		if (firstSubmission) {
			
			String name = "";
			if(Magic.validString(request.getParameter("name"))) {
				 name = request.getParameter("name");
			} else {
				errors.add("Name was empty");
			}
			
			String location = "";
			if(Magic.validString(request.getParameter("location"))) {
				location = request.getParameter("location");
			} else {
				errors.add("Location was empty");
			}
			
			if(!errors.isEmpty()) {
				request.setAttribute("errors", errors);
				
				Action redirect = null;
				try {
					redirect = new addHotelGet();
				} catch (ServletException e) {
					e.printStackTrace();
				}
				return redirect.execute(request, response);
			}
			
			HotelDTO hotel = new HotelDTO(name,location);
			hotelChain.newHotel(hotel);
			
		}
		
		Action redirect = null;
		try {
			redirect = new hotelList();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		return redirect.execute(request, response);

	}

}
