package com.groupN.actions.owner;

import java.sql.SQLException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelDTO;

import java.util.List;

public class hotelList implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(hotelList.class.getName());

	
	public hotelList() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		List<HotelDTO> hotels = hotelChain.findAll();
		request.setAttribute("hotels", hotels);
		
		
		return "/WEB-INF/restricted/owner/hotelList.jsp";
	}

}
