package com.groupN.actions.owner;

import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.common.ModifiableRequest;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelDTO;
import com.groupN.jdbc.HotelRoomDTO;

public class updateStatusPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(updateStatusPost.class.getName());

	
	public updateStatusPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}
	
	
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		int hotelID = Integer.parseInt(request.getParameter("id"));
		
			String[] maintenance = request.getParameterValues("maintenance");
				logger.info("sup");
			for (int i = 0; i < maintenance.length; i++) {	
				HotelRoomDTO hr = null;
				String status = "Available";

				try {
						hr = hotelChain.findHotelRoom(Integer.parseInt(maintenance[i]));
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (EmptyResultException e) {
						e.printStackTrace();
					}
					
					if (hr.getStatus().equals("Available")) {
						status = "Under maintenance";
					}
					
					hotelChain.updateRoomStatus(hr.getID(), status);
			}

			//THe amazing 
			Map<String, String[]> extraParams = new TreeMap<String, String[]>();
			extraParams.put("id", new String[]{""+hotelID});
			HttpServletRequest wrappedRequest = new ModifiableRequest(request, extraParams);
			
			Action redirect=null;
			try {
				redirect = new showHotel();
			} catch (ServletException e) {
				e.printStackTrace();
			}
			return redirect.execute(wrappedRequest, response); 
			
			
			
	
	}

}
