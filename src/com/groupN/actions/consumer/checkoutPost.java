package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.ResultBean;
import com.groupN.jdbc.ResultDTO;

public class checkoutPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(checkoutPost.class.getName());

	
	public checkoutPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}


	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		
		String token = request.getParameter("token");

		boolean firstSubmission = hotelChain.goodToken(token);
		//TODO: Actually Dont save if !firstSubmission and just render result page
		
		if (firstSubmission) {
			BookingDTO booking = (BookingDTO) request.getSession().getAttribute("booking");
			ResultBean resultBean = (ResultBean) request.getSession().getAttribute("resultBean");
			

			String[] extraBeds = request.getParameterValues("extraBed");
			int price = 0;
			
			if (extraBeds != null) {
				for (int i = 0; i < extraBeds.length; i++) {
					
					int index = resultBean.getSelected().get(Integer.parseInt(extraBeds[i]));
					ResultDTO r =  resultBean.getResult(index);
					
					r.setExtraBed(true);
					resultBean.setResult(index, r);
					
					
				}
			}
			
			for (ResultDTO r : resultBean.getResultsSelected()) {
				price+= r.getTotalPrice();
			}
			booking.setPrice(price);
			

	
			request.getSession().setAttribute("booking", booking);
			request.getSession().setAttribute("resultBean", resultBean);
		}
		
		String token1 = hotelChain.newFormToken("coToken");
		request.setAttribute("token", token1);
		
		return "checkout.jsp";
	}

}
