package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.common.Magic;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.BookingRoomDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.ResultBean;
import com.groupN.jdbc.ResultDTO;
import com.groupN.mail.MailSender;

public class paymentMadePost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(paymentMadePost.class.getName());

	
	public paymentMadePost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}
	
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		

		List<String> errors = new ArrayList<String>();
		
		String token = request.getParameter("token");

		boolean firstSubmission = hotelChain.goodToken(token);		
		String[] name = request.getParameterValues("name");
		String[] email = request.getParameterValues("email");
		System.out.println("GOT TOKEN + " + token );
		
		if (firstSubmission) {
			boolean validEmail = Magic.validEmail(email[0]);

			if (name[0].equals("")|| email[0].equals("") || !validEmail) {
				if (name[0].equals(""))
					errors.add("Please provide a name");
				if (email[0].equals(""))
					errors.add("Please provide an email address");
				if (!validEmail)
					errors.add("Please provide a valid email address");
				
				try {
					Action redirect = new payPost();
					request.setAttribute("errors", errors);
					return redirect.execute(request, response);
				} catch (ServletException e) {
					e.printStackTrace();
				}
			}
			
		
			BookingDTO booking = (BookingDTO) request.getSession().getAttribute("booking");
			
			ResultBean resultBean = (ResultBean) request.getSession().getAttribute("resultBean");
			
			

			
			booking.setEmail(email[0]);
			booking.setName(name[0]);
//			booking.setPin(UUID.randomUUID().toString());
			booking.setPin("1234567");
			booking.setRoomBookings(new ArrayList<BookingRoomDTO>());
						
			logger.info(booking.getEmail() + " "+ booking.getName()+ " "+ booking.getPin());
			
			for (ResultDTO r: resultBean.getResultsSelected()) {
				BookingRoomDTO br = new BookingRoomDTO();
				br.setBooking(booking);
				br.setExtraBed(r.isExtraBed());
				br.setHotel(r.getHotel());
				br.setRoom(r.getRoom());
				
				booking.add(br);
				
			}
			
			int id = hotelChain.newBooking(booking);
			booking.setID(id);
			request.getSession().setAttribute("booking", booking);

			
			/*
			System.out.println("Saved Booking Now trying to send email");
			
			MailSender sender = null;
			try{
				System.out.println("Initilaiseing sender");

				sender = MailSender.getMailSender();
				
				System.out.println("Sender initialised  ");

				
				sender.sendMessage(booking);
			}catch (Exception e){
				logger.severe("Oopsies, could not send message "+e.getMessage());
				e.printStackTrace();
			}
			
			System.out.println(id);
			*/

		}
		
		String token1 = hotelChain.newFormToken("pmPay");
		request.setAttribute("token", token1);	

		
		return "thankYou.jsp";
		
	}

}
