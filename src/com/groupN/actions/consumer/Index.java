package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;

public class Index implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(Index.class.getName());

	
	public Index() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}
	
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
				
		String token = hotelChain.newFormToken("consumerIndex");
		request.setAttribute("token", token);
		
		
		
		return "search.jsp";
	}

}
