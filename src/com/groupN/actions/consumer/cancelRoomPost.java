package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.common.ModifiableRequest;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;

public class cancelRoomPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(cancelRoomPost.class.getName());

	
	public cancelRoomPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response){
		
		Action redirect=null;
		BookingDTO booking = (BookingDTO) request.getSession().getAttribute("currentBooking");
		int id = Integer.parseInt(request.getParameter("id"));
		
		try {
			booking = hotelChain.getBooking(id);
		} catch (EmptyResultException e) {
			e.printStackTrace();
		}
		request.getSession().setAttribute("currentBooking", booking);
		
		String token1 = hotelChain.newFormToken("crPay");
		request.setAttribute("token", token1);	
		//THe amazing 
		Map<String, String[]> extraParams = new TreeMap<String, String[]>();
		extraParams.put("id", new String[]{""+id});
		HttpServletRequest wrappedRequest = new ModifiableRequest(request, extraParams);
		
		try {
			redirect = new viewBooking();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		
		return redirect.execute(wrappedRequest, response);

	}

}
