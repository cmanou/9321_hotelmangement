package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;

public class viewBooking implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(viewBooking.class.getName());

	
	public viewBooking() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		List<String> errors = new ArrayList<String>();
		
		String token = request.getParameter("token");

		boolean firstSubmission = hotelChain.goodToken(token);
		//TODO: Actually Dont save if !firstSubmission and just render result page
		
		
		
		int id = Integer.parseInt(request.getParameter("id"));
		String userPin = request.getParameter("password");
		BookingDTO currentBooking = (BookingDTO) request.getSession().getAttribute("currentBooking");

		BookingDTO booking = null;
		try {
			booking = hotelChain.getBooking(id);
		} catch (EmptyResultException e) {
			e.printStackTrace();
		}
		
		if(booking.getPin().equals(userPin)) {
			request.getSession().setAttribute("currentBooking", booking);
			return "/bookingView.jsp";			
		} else if (currentBooking != null) {
			request.getSession().setAttribute("currentBooking", booking);
			return "/bookingView.jsp";
		} else {
			request.setAttribute("id",id);
			String error = "Incorrect Password or URL";
			request.setAttribute("error",error );
			Action bk=null;
			try {
				bk = new viewBookingLogin();
			} catch (ServletException e) {
				e.printStackTrace();
			}
			return bk.execute(request, response);
		}
		

	}

}
