package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;

public class cancelPost implements Action {
	private HotelChainDAO hotel;
	static Logger logger = Logger.getLogger(cancelPost.class.getName());

	
	public cancelPost() throws ServletException {
		super();
		try {
			hotel = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response){
		
		Action redirect=null;
		try {
			redirect = new Index();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		request.getSession().invalidate();
		
		return redirect.execute(request, response);

	}

}
