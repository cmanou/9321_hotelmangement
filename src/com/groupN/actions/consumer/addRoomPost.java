package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.ResultBean;
import com.groupN.jdbc.ResultDTO;

public class addRoomPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(addRoomPost.class.getName());

	
	public addRoomPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}


	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {

		List<String> errors = new ArrayList<String>();
		
		String token = request.getParameter("token");
		boolean firstSubmission = hotelChain.goodToken(token);

		if (firstSubmission) {
			BookingDTO booking = (BookingDTO) request.getSession().getAttribute("currentBooking");
			ResultBean resultBean = (ResultBean) request.getSession().getAttribute("resultBean");
			

			String[] extraBeds = request.getParameterValues("extraBed");
			
			
			if (extraBeds != null) {
				for (int i = 0; i < extraBeds.length; i++) {
					
					int index = resultBean.getSelected().get(Integer.parseInt(extraBeds[i]));
					ResultDTO r =  resultBean.getResult(index);
					
					r.setExtraBed(true);
					resultBean.setResult(index, r);
					
					
				}
			}
			
			int price = booking.getPrice();
			for (ResultDTO r : resultBean.getResultsSelected()) {
				price+= r.getTotalPrice();
			}
			booking.setPrice(price);
			
			request.getSession().setAttribute("currentBooking", booking);
			request.getSession().setAttribute("resultBean", resultBean);
		}

		
		return "addRoom.jsp";
	}

}
