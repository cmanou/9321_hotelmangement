package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.EmptyResultException;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.BookingRoomDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelDTO;
import com.groupN.jdbc.ResultBean;
import com.groupN.jdbc.ResultDTO;

public class confirmRoomsPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(confirmRoomsPost.class.getName());
	
	public confirmRoomsPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}


	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) { 
		
		String token = request.getParameter("token");
		boolean firstSubmission = hotelChain.goodToken(token);
		
		BookingDTO booking = (BookingDTO) request.getSession().getAttribute("booking");
		ResultBean resultBean = (ResultBean) request.getSession().getAttribute("resultBean");
		
		String[] selectRooms = request.getParameterValues("roomsSelected");
		List<String> errors = new ArrayList<String>();
		
		if (selectRooms == null) {
			Action redirect=null;
			try {
				redirect = new Index();
			} catch (ServletException e) {
				e.printStackTrace();
			}
			request.setAttribute("errors", "Please select a room to book");
			return redirect.execute(request, response);
		}
		
		if (firstSubmission) {
			List<Integer> selected = resultBean.getSelected();
			int price = 0;
			
			for (int i = 0; i < selectRooms.length; i++) {
				selected.add(Integer.parseInt(selectRooms[i]));
			}
			
			resultBean.setSelected(selected);
			
			for (ResultDTO r : resultBean.getResultsSelected()) {
				price+= r.getTotalPrice();
			}
			booking.setPrice(price);
			
			request.getSession().setAttribute("resultBean", resultBean);
			request.getSession().setAttribute("booking", booking);
		}
		
		
		String token1 = hotelChain.newFormToken("crToken");
		request.setAttribute("token", token1);
		
		return "confirmRooms.jsp";
	}

}
