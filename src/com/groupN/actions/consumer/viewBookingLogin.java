package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;

public class viewBookingLogin implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(viewBookingLogin.class.getName());
	
	public viewBookingLogin() throws ServletException {
		super();
		
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		int id = Integer.parseInt(request.getParameter("id"));
		String token = hotelChain.newFormToken("bookingLogin");
		request.setAttribute("token", token);
		boolean yes = hotelChain.canEditBooking(id);
		
		if (yes) {
			request.setAttribute("id", id);
			return "/bookingLogin.jsp";
		} else {
			return "/bookingDenied.jsp";
		}
	}
}
