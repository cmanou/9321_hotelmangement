package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.BookingRoomDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.ResultBean;
import com.groupN.jdbc.ResultDTO;

public class bookRoomsPost implements Action{
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(bookRoomsPost.class.getName());

	
	public bookRoomsPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}


	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {

		List<String> errors = new ArrayList<String>();
		
		String token = request.getParameter("token");

		boolean firstSubmission = hotelChain.goodToken(token);
		//TODO: Actually Dont save if !firstSubmission and just render result page
		
		BookingDTO booking = (BookingDTO) request.getSession().getAttribute("currentBooking");
		ResultBean resultBean = (ResultBean) request.getSession().getAttribute("resultBean");
		
		for (ResultDTO r: resultBean.getResultsSelected()) {
			BookingRoomDTO br = new BookingRoomDTO();
			br.setBooking(booking);
			br.setExtraBed(r.isExtraBed());
			br.setHotel(r.getHotel());
			br.setRoom(r.getRoom());
			
			booking.add(br);
			
		}
		
		
		System.out.print("price "+booking.getPrice());
		int customerID = Integer.parseInt(request.getParameter("id"));
		hotelChain.updateBooking(resultBean.getResultsSelected(), customerID, booking.getPrice());
		
		resultBean = new ResultBean();
		request.getSession().setAttribute("resultBean", resultBean);
		request.setAttribute("id", customerID);
		
		try {
			Action redirect = new viewBooking();
			return redirect.execute(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		}
		
		String token1 = hotelChain.newFormToken("bmPay");
		request.setAttribute("token", token1);	
		
		return "bookingView.jsp";
	}

}
