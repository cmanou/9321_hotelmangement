package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.BookingRoomDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.HotelDTO;
import com.groupN.jdbc.ResultBean;
import com.groupN.jdbc.ResultDTO;

public class searchRoomPost implements Action{
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(searchRoomPost.class.getName());

	
	public searchRoomPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}
	
	
	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		List<String> errors = new ArrayList<String>();
		
		String token = request.getParameter("token");

		boolean firstSubmission = hotelChain.goodToken(token);
		//TODO: Actually Dont save if !firstSubmission and just render result page
		
		
		BookingDTO currentBooking = (BookingDTO) request.getSession().getAttribute("currentBooking");
		List<BookingRoomDTO> rooms = currentBooking.getRoomBookings();
		HotelDTO hotel = null;
			
		for (BookingRoomDTO br: rooms) {
			hotel = br.getHotel();
		}
		
		logger.info("hotel id is: "+hotel.getID());
		System.out.print("lovation "+hotel.getLocation()+ " current "+currentBooking.getStartDate()+ " end "+currentBooking.getEndDate());
		
		float max = 3000;
		List<ResultDTO> roomsAvail = hotelChain.findAvaliablitly(hotel.getLocation(), currentBooking.getStartDate(), currentBooking.getEndDate(),max, 1);
		System.out.println("roomsAvail "+roomsAvail.size());
		
		ResultBean resultBean = (ResultBean) request.getSession().getAttribute("resultBean");
		resultBean.setResults(roomsAvail);
		request.getSession().setAttribute("resultBean", resultBean);				
		
		String token1 = hotelChain.newFormToken("srPost2");
		request.setAttribute("token", token1);
		
		return "searchRooms.jsp";
	}

}
