package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.ResultBean;

public class confirmExtraRoomsPost implements Action {
	
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(confirmExtraRoomsPost.class.getName());

	
	public confirmExtraRoomsPost() throws ServletException {
		super();
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}


	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		

		List<String> errors = new ArrayList<String>();
		String token = request.getParameter("token");
		boolean firstSubmission = hotelChain.goodToken(token);
		String[] selectRooms = request.getParameterValues("roomsSelected");
		
		BookingDTO currentBooking = (BookingDTO) request.getSession().getAttribute("currentBooking");
		ResultBean resultBean = (ResultBean) request.getSession().getAttribute("resultBean");
		
		if (selectRooms == null) {
			Action redirect=null;
			try {
				redirect = new viewBooking();
			} catch (ServletException e) {
				e.printStackTrace();
			}
			request.setAttribute("errors", "Please select a room to book");
			return redirect.execute(request, response);
		}
		
		if (firstSubmission) {
			List<Integer> selected = resultBean.getSelected();

			for (int i = 0; i < selectRooms.length; i++) {
				selected.add(Integer.parseInt(selectRooms[i]));
			}
			
			resultBean.setSelected(selected);
			
			request.getSession().setAttribute("resultBean", resultBean);
			request.getSession().setAttribute("booking", currentBooking);
		}
		
		String token1 = hotelChain.newFormToken("cerToken");
		request.setAttribute("token", token1);
		
		return "confirmExtraRoom.jsp";
	}

}
