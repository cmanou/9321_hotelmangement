package com.groupN.actions.consumer;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.groupN.actions.Action;
import com.groupN.common.Magic;
import com.groupN.exceptions.ServiceLocatorException;
import com.groupN.jdbc.BookingDTO;
import com.groupN.jdbc.DerbyDAOImpl;
import com.groupN.jdbc.HotelChainDAO;
import com.groupN.jdbc.ResultBean;
import com.groupN.jdbc.ResultDTO;


public class searchResultsPost implements Action {
	private HotelChainDAO hotelChain;
	static Logger logger = Logger.getLogger(searchResultsPost.class.getName());
	private DateFormat df;
	
	public searchResultsPost() throws ServletException {
		super();
		df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
		
		try {
			hotelChain = new DerbyDAOImpl();
		} catch (ServiceLocatorException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		} catch (SQLException e) {
			logger.severe("Trouble connecting to database "+e.getStackTrace());
			throw new ServletException();
		}
	}

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		

		List<String> errors = new ArrayList<String>();
		
		String token = request.getParameter("token");

		boolean firstSubmission = hotelChain.goodToken(token);
		//TODO: Actually Dont save if !firstSubmission and just render result page
		
		
		
		String city = "";
		float maxPrice = 0;
		int numRooms = 0;
		Date startDate = null, endDate = null;
		Date now = new Date();
		
		//TODO: Validation
		
		if (!Magic.validString(request.getParameter("city"))) {
			errors.add("Please provide a city");
		} else {
			city = request.getParameter("city");
		}
		
		if (!Magic.validInteger(request.getParameter("numRooms"))) {
			errors.add("Invalid number of rooms needed");
		} else {
			numRooms = Integer.parseInt(request.getParameter("numRooms"));
		}
		
		if (!Magic.validFloat(request.getParameter("maxPrice"))) {
			errors.add("Invalid Maximum Price per Room");
		} else {
			maxPrice = Float.parseFloat(request.getParameter("maxPrice"));
		}
		
		
		if(!Magic.validDate(request.getParameter("startDate"))) {
			errors.add("Invalid Start Date");
		} else {
			startDate = Magic.parseDate(request.getParameter("startDate"));
			if (startDate.before(now))
				errors.add("Invalid Start Date");
		}
		
		if(!Magic.validDate(request.getParameter("endDate"))) {
			errors.add("Invalid End Date");
		} else {
			endDate = Magic.parseDate(request.getParameter("endDate"));
			if (endDate.before(now))
				errors.add("Invalid End Date");
		}
		

		if (errors.isEmpty()) {
			List<ResultDTO> roomsAvail = hotelChain.findAvaliablitly(city, startDate, endDate, maxPrice, numRooms);
			
			ResultBean resultBean = (ResultBean) request.getSession().getAttribute("resultBean");
			
			if (resultBean == null)
				resultBean = new ResultBean();
			
			resultBean.setResults(roomsAvail);
			request.getSession().setAttribute("resultBean", resultBean);
			
			BookingDTO booking = (BookingDTO) request.getSession().getAttribute("booking");
			if(booking==null) 
				booking = new BookingDTO();
			booking.setEndDate(endDate);
			booking.setStartDate(startDate);
			request.getSession().setAttribute("booking", booking);
		
			
			String token1 = hotelChain.newFormToken("srPost");
			request.setAttribute("token", token1);
			
			return "searchResults.jsp";
		} else {
			request.setAttribute("errors", errors);
			Action redirect = null;
			try {
				redirect = new Index();
			} catch (ServletException e) {
				e.printStackTrace();
			}
			return redirect.execute(request, response); 
		}
		
	}
}
