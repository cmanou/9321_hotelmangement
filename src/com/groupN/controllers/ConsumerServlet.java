package com.groupN.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.groupN.actions.Action;
import com.groupN.actions.Logout;
import com.groupN.actions.consumer.*;


@WebServlet( name="ConsumerServlet", displayName="Consumer Servlet", urlPatterns = {"/consumer/*"}, loadOnStartup=1)
public class ConsumerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Map<String, Action> actions;
	static Logger logger = Logger.getLogger(ConsumerServlet.class.getName());

	
	public ConsumerServlet () throws ServletException {
		super.init();
		logger.info("Loaded COnsumer servlet");
		actions = new HashMap<String, Action>();
	    actions.put("GET/", new Index());
	    
	    actions.put("GET/search", new Index());
	    actions.put("POST/searchResults", new searchResultsPost());
	    
	    actions.put("POST/confirmRooms", new confirmRoomsPost());
	    
	    actions.put("POST/checkout", new checkoutPost());
	    
	    actions.put("POST/pay", new payPost());
	    
	    actions.put("POST/paymentMade", new paymentMadePost());
	    
	    actions.put("POST/cancel", new cancelPost());
	    actions.put("POST/cancelRoom", new cancelRoomPost());
	    
	    actions.put("GET/viewBooking", new viewBookingLogin());
	    actions.put("POST/viewBooking", new viewBooking());
	    
	    actions.put("POST/searchRoom", new searchRoomPost());
	    
	    actions.put("POST/confirmExtraRooms", new confirmExtraRoomsPost());

	    actions.put("POST/addRoom", new addRoomPost());
	    
	    actions.put("POST/bookRooms", new bookRoomsPost());

	    actions.put("GET/logout", new Logout());

	       
		logger.info("FInished Loading COnsumer servlet");

	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Action action = getAction(request);
	    String view;
	    
	    if(action != null) {
	    	view = action.execute(request, response);
	    } else {
	    	view = "search.jsp";
	    }
	    
	    RequestDispatcher rd = request.getRequestDispatcher("/"+view);
		rd.forward(request, response);
	}
	
	public static Action getAction(HttpServletRequest request) {
		logger.info(request.getMethod() + request.getPathInfo());
		return actions.get(request.getMethod() + request.getPathInfo());
	}
	
}
