package com.groupN.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.groupN.actions.Action;
import com.groupN.actions.manager.*;

@WebServlet( name="ManagerServlet", displayName="Manager Servlet", urlPatterns = {"/manager/*"}, loadOnStartup=1)
public class ManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Map<String, Action> actions;
	static Logger logger = Logger.getLogger(ManagerServlet.class.getName());

	
	@Override
	public void init() throws ServletException {
		   super.init();
	       logger.info("Loading managerr");
	       actions = new HashMap<String, Action>();
	       actions.put("GET/", new Index());

	       actions.put("GET/hotelView", new hotelView());
	       
	       actions.put("GET/checkin", new CheckinGet());
	       actions.put("POST/checkin", new CheckinPost());

	       actions.put("GET/checkout", new CheckoutGet());
	       actions.put("POST/checkout", new CheckoutPost());

	       
	       logger.info("FINISHED manager INIT");
	}
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Action action = getAction(request);
	    String view;
	    
	    if(action != null) {
	    	view = action.execute(request, response);
	    } else {
	    	view = actions.get("GET/").execute(request, response);
	    }
		
		RequestDispatcher rd = request.getRequestDispatcher(view);
		rd.forward(request, response);

	}
	
	public static Action getAction(HttpServletRequest request) {
		return actions.get(request.getMethod() + request.getPathInfo());
	}
	
}
