package com.groupN.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.groupN.actions.Action;
import com.groupN.actions.Logout;
import com.groupN.actions.owner.*;

@WebServlet( name="HotelOwnerServlet", displayName="Hotel Owner Servlet", urlPatterns = {"/owner/*"}, loadOnStartup=1)
public class HotelOwnerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Map<String, Action> actions;
	static Logger logger = Logger.getLogger(HotelOwnerServlet.class.getName());

	
	@Override
	public void init() throws ServletException {
		   super.init();
		   
	       logger.info("Loading HOTEL OWNER INIT");
	       
	       actions = new HashMap<String, Action>();
	       
	       actions.put("GET/", new hotelList());

	       actions.put("GET/showHotel", new showHotel());

	       actions.put("GET/addHotel", new addHotelGet());
	       actions.put("POST/addHotel", new addHotelPost());
	       
	       actions.put("GET/addRoom", new addRoomGet());
	       actions.put("POST/addRoom", new addRoomPost());
	       
	       actions.put("GET/newDiscount", new newDiscountGet());
	       actions.put("POST/newDiscount", new newDiscountPost());
	       
	       actions.put("POST/updateStatus", new updateStatusPost());
	       
	       actions.put("GET/logout", new Logout());

	       logger.info("FINISHED HOTEL OWNER INIT");
	}
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Action action = getAction(request);
	    String view;
	    
	    if(action != null) {
	    	view = action.execute(request, response);
	    } else {
	    	view = new hotelList().execute(request, response);
	    }
		
		RequestDispatcher rd = request.getRequestDispatcher(view);
		rd.forward(request, response);

	}
	
	public static Action getAction(HttpServletRequest request) {
		return actions.get(request.getMethod() + request.getPathInfo());
	}
	
}
